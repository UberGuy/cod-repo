/*jshint esversion: 6 */

Number.prototype.toPlaces = function (places) {

  if (places === undefined) places = 2;
  let mult = Math.pow(10, places);
  let result = Math.round(this * mult) / mult;
  // Is the integer part of this a single-digit number?
  if (Math.abs(result) < 10) {
    // Is this a (near) integer result?
      if (Math.abs(result - Math.trunc(result)) < 0.001) {
      // If so, add at least one significant digit
      // So 1 becomes 1.0, but 1.23 remains 1.23.
      return result.toFixed(1);
    }
  }
  return result;
};

Number.prototype.fixedWidth = function (width, places) {

  const rounded = Math.round(this);
  const digits = rounded.toString().length;
  const diff = width - digits;
  if (diff === 0) {
    return rounded;
  }
  if (diff < 0) {
    return rounded.toExponential(width - 4);
  }
  if (places === undefined) {
    return this.toPlaces(diff);
  }
  return this.toPlaces(diff < places ? diff : places);
};
