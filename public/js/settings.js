/*jshint esversion: 8 */

import "/js/storage_shim.js";

export const STORAGE_MAP = {
  display_100_pct: ["display-100-pct", true],
  show_raw_data: ["show_raw_data", false],
  always_prefer_at_none: ["always-prefer-at-none", false],
  preferred_default_at: ["preferred-default-at", 'none'],
  show_pvp_or_pve: ["show_pvp_or_pve", false],
  show_pvp_and_pve: ["show_pvp_and_pve", true],
  data_path: ["data_path", null],
};

export function get_setting(key, default_override) {

  if (key in STORAGE_MAP) {
    const [storage_key, default_val] = STORAGE_MAP[key];
    const value = localStorage.getObject(storage_key);
    if (value === null) {
      if (default_override === undefined) {
        return default_val;
      }
      else {
        return default_override;
      }
    }
    return value;
  }
  throw `Invalid setting name: ${key}`;
}

export function set_setting(key, value) {

  if (key in STORAGE_MAP) {
    const [storage_key, _] = STORAGE_MAP[key];
    localStorage.setObject(storage_key, value);
    return true;
  }
  throw `Invalid setting name: ${key}`;
}
