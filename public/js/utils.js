/*jshint esversion: 8 */

// Return true if a variable is array-like (has a .length)
// and has a length > 0. Else return false.
export function checkArray(array) {

  return (Array.isArray(array) && (array.length > 0));
}

// Home-grown `typeof` which avoids some of the limitations of `typeof`
export function typeOf(obj) {

  return Object.prototype.toString.call(obj).match(/\s(\w+)/)[1].toLowerCase();
}

// Retrieve a component template's raw HTML
export const fetch_template = async (component_name) => {

  let template_file = `/components/${component_name}/comp.html`;
  let response = await fetch(template_file);
  let template = await response.text();
  return template;
};

export function setsEqual(a, b) {
  return (a.size === (b.size || b.length)) && [...b].every(element => a.has(element));
}

export function fetchData(url, action) {

  return fetch(url, { "method": "GET" })
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        console.log("Server returned " + response.status + " : " + response.statusText);
      }
    })
    .then(function(response) {
      action(response);
    })
    .catch(err => {
      console.log(err);
    });
}

export function snake_to_ucamel_case(str) {

    const parts = str.split("_");
    return parts.map((x) => {
      return x[0].toUpperCase() + x.substring(1);
    }).join(" ");
  }

export function capitalize_words(text) {
   return text.replace(
     /\w\S*/g,
     (txt) => txt.charAt(0).toUpperCase() + txt.substr(1)
   );
}

export function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
}

export function int_array_to_ranges(numbers) {

  let rstart, rend;
  const ranges = [];

  const array = [...numbers];

  for (let i = 0; i < array.length; i++) {
    rstart = array[i];
    rend = rstart;
    while (array[i + 1] - array[i] == 1) {
      rend = array[i + 1]; // increment the index if the numbers sequential
      i++;
    }
    ranges.push(rstart == rend ? rstart + "" : rstart + "-" + rend);
  }
  return ranges;
}
