/*jshint esversion: 8 */

import { CONFIG } from "/config/config.js";

export default {

  methods: {
    powercat_url(pcat_name, at_name) {
      const lc_pcat_name = pcat_name.toLowerCase();
      if (at_name) {
        const lc_at_name = at_name.toLowerCase();
        return `/html/category.html?pcat=${lc_pcat_name}&at=${lc_at_name}`;
      }
      return `/html/category.html?pcat=${lc_pcat_name}`;
    },
    powerset_url(pset_name, at_name) {
      const lc_pset_name = pset_name.toLowerCase();
      if (at_name) {
        const lc_at_name = at_name.toLowerCase();
        return `/html/powerset.html?pset=${lc_pset_name}&at=${lc_at_name}`;
      }
      return `/html/powerset.html?pset=${lc_pset_name}`;
    },
    power_url(pow_name, at_name) {
      const lc_pow_name = pow_name.toLowerCase();
      if (at_name) {
        const lc_at_name = at_name.toLowerCase();
        return `/html/power.html?power=${lc_pow_name}&at=${lc_at_name}`;
      }
      return `/html/power.html?power=${lc_pow_name}`;
    },
    tag_url(tag_name, lookup_type, at_name) {
      const lc_tag_name = tag_name.toLowerCase();
      const lc_lookup_type = lookup_type.toLowerCase();
      const query = (lc_lookup_type === "affects") ? "affects" : "bears";
      if (at_name) {
        const lc_at_name = at_name.toLowerCase();
        return `/html/tags.html?tag=${lc_tag_name}&q=${query}&at=${lc_at_name}`;
      }
      return `/html/tags.html?tag=${tag_name}&q=${query}`;
    },
    at_url(at_name) {
      const lc_at_name = at_name.toLowerCase();
      return `/html/archetype.html?at=${lc_at_name}`;
    },
    at_details_url(at_name) {
      if (at_name) {
        const lc_at_name = at_name.toLowerCase();
        return `/html/archetype-data.html?at=${lc_at_name}`;
      }
      return "/html/archetype-data.html";
    },
    entity_url(entity_name) {
      if (entity_name) {
        const lc_entity_name = entity_name.toLowerCase();
        return `/html/entity.html?entity=${lc_entity_name}`;
      }
      return "/html/entity.html";
    },
    boostset_url(boostset_name) {
      if (boostset_name) {
        const lc_boostset_name = boostset_name.toLowerCase();
        return `/html/boostset.html?set=${lc_boostset_name}`;
      }
      return "/html/boostset.html";
    },
    boostset_group_url(group_name) {
      if (group_name) {
        const lc_group_name = group_name.toLowerCase();
        return `/html/boostset-groups.html?group=${lc_group_name}`;
      }
      return "/html/boostset-groups.html";
    },
    mods_table_url() {
      return "/html/modifiers-table.html";
    },
    search_url(power_fullname) {
      const lc_power_fullname = power_fullname.toLowerCase();
      return `/html/index.html?search=${lc_power_fullname}`;
    },
    home_url() {
      return `/html/index.html`;
    },
    settings_url() {
      return `/html/settings.html`;
    },
    help_url(page_name) {
      if (page_name) {
        const lc_page_name = page_name.toLowerCase();
        return `/html/help.html?page=${lc_page_name}`;
      }
      return "/html/help.html";
    },
    help_url_raw(page_name) {
      return `/html/help/${page_name}.html`;
    },

    asset_url(asset_name) {
      const lc_asset_name = asset_name.toLowerCase();
      return `/assets/${lc_asset_name}`;
    },

    base_data_url(file_name) {
      return `${CONFIG.data_path}/${file_name}.json`;
    },
    pcat_data_url(pcat_name) {
      const lc_pcat_name = pcat_name.toLowerCase();
      return `${CONFIG.data_path}/powers/${lc_pcat_name}/index.json`;
    },
    pset_data_url(pcat_name, pset_name) {
      const lc_pcat_name = pcat_name.toLowerCase();
      const lc_pset_name = pset_name.toLowerCase();
      return `${CONFIG.data_path}/powers/${lc_pcat_name}/${lc_pset_name}/index.json`;
    },
    power_data_url(pcat_name, pset_name, power_name) {
      const lc_pcat_name = pcat_name.toLowerCase();
      const lc_pset_name = pset_name.toLowerCase();
      const lc_power_name = power_name.toLowerCase();
      return `${CONFIG.data_path}/powers/${lc_pcat_name}/${lc_pset_name}/${lc_power_name}.json`;
    },
    at_index_url() {
      return `${CONFIG.data_path}/archetypes/index.json`;
    },
    at_data_url(at_name) {
      const lc_at_name = at_name.toLowerCase();
      return `${CONFIG.data_path}/archetypes/${lc_at_name}.json`;
    },
    mod_table_url(at_name) {
      const lc_at_name = at_name.toLowerCase();
      return `${CONFIG.data_path}/tables/${lc_at_name}.json`;
    },
    tag_data_url(tag_name) {
      const lc_tag_name = tag_name.toLowerCase();
      return `${CONFIG.data_path}/tags/${lc_tag_name}.json`;
    },
    entity_data_url(entity_name) {
      const lc_entity_name = entity_name.toLowerCase();
      return `${CONFIG.data_path}/entities/${lc_entity_name}.json`;
    },
    boostset_index_url() {
      return `${CONFIG.data_path}/boost_sets/index.json`;
    },
    boostset_data_url(boostset_name) {
      const lc_boostset_name = boostset_name.toLowerCase();
      return `${CONFIG.data_path}/boost_sets/${lc_boostset_name}.json`;
    },
    boostset_icon_url(icon_name) {
      const lc_icon_name = icon_name.toLowerCase();
      return `${CONFIG.data_path}/icons/special/${lc_icon_name}`;
    },
    base_icon_url(icon_name) {
      const lc_icon_name = icon_name.toLowerCase();
      return `${CONFIG.data_path}/icons/${lc_icon_name}`;
    },
    boost_icon_url(icon_name) {
      const lc_icon_name = icon_name.toLowerCase();
      return `${CONFIG.data_path}/icons/generics/${lc_icon_name}`;
    },
  },
};
