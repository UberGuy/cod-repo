/*jshint esversion: 8 */

import {fetch_template} from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";
import tippy_mixin from "/components/cod-tippy/libs/tippy_mixin.js";

const TAGS_TO_IGNORE = new Set([
  "Accuracy", "Boost", "Damage", "DamageProc", "Debuff_Dam", "Defense",
  "EndDrain", "Endurance", "FastSnipe", "Heal", "InterruptTime", "Knock",
  "KnockProc", "Mez", "Movement", "Perception", "Range", "Regen",
  "Regeneration", "Res", "Status", "Stealth", "Terrorize", "ToHit",
  "rechargetime", "Ones",
]);


// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await fetch_template("cod-effect-icons");
  return {
    name: "effect-icons",
    template: template,
    mixins: [urls_mixin, tippy_mixin],
    props: {
      effect_data: {
        type: Object,
        required: true,
      },
    },
    data() {
      return {
        ready: false,
      };
    },
    methods: {
    },
    computed: {
      filteredTags() {
        return this.effect_data.tags.filter(x => !TAGS_TO_IGNORE.has(x));
      },
      effectFlags() {
        return this.effect_data.flags;
      },
      tagsContent() {
        if (this.ready && this.filteredTags.length) {
          return this.$refs.tags.innerHTML;
        }
        return false;
      },
      radiusInnerContent() {
        if (this.ready && this.effect_data.radius_inner !== -1) {
          return this.$refs.radiusInner.innerHTML;
        }
        return false;
      },
      radiusOuterContent() {
        if (this.ready && this.effect_data.radius_outer !== -1) {
          return this.$refs.radiusOuter.innerHTML;
        }
        return false;
      },
      mainTargetOnly() {
        return this.ready && this.effectFlags.includes("MainTargetOnly");
      },
      secondaryTargetsOnly() {
        return this.ready && this.effectFlags.includes("SecondaryTargetsOnly");
      },
      hidden() {
        return this.ready && this.effectFlags.includes("HideFromInfo");
      },
    },
    mounted() {
      this.$nextTick(function () {
        this.ready = true;
      });
    },
  };
};
