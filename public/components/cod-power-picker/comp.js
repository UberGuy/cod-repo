/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

import SearchPicker from "/components/search-picker/comp.js";

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-power-picker");
  return {
    name: "cod-power-picker",
    template: template,
    mixins: [urls_mixin],
    components: {
      "search-picker": SearchPicker,
    },
    data() {
      return {
        all_powers: null,
        dropdown_data: undefined,
        initial_value: null,
        search_param: null,
        power_fullname: null,
        ready: false,
        picker_ready: false,
      };
    },
    props: {
      max_items: {
        type: Number,
        default: 20,
      },
    },
    computed: {
    },
    methods: {
      setSelected(picker_obj) {
        console.log("Selecting", picker_obj.value);
        this.power_fullname = picker_obj.value;
        this.setHistory();
        this.$emit("selected-power-changed", picker_obj);
      },
      setHistory() {
        if (this.power_fullname !== null) {
          this.power_fullname = this.power_fullname.toLowerCase();
          let current_loc = window.location.pathname + window.location.search;
          let to_push = this.search_url(this.power_fullname);
          // Don't create a new history entry every time we change powers
          window.history.replaceState(this.powerset_path, null, to_push);
        }
      },
      processPowers(data) {
        let options = [];
        for (const entry of this.all_powers) {
          options.push({
            value: entry[1],
            option: entry[0],
          });
        }
        this.all_powers = null;
        this.dropdown_data =  options.sort((a, b) => {
          if (a.option > b.option) return 1;
          if (a.option < b.option) return -1;
          return 0;
        });
      },
      itemFromValue(value) {
        for (const item of this.dropdown_data) {
          if (item.value === value) {
            return item;
          }
        }
        return null;
      },
      fetchData() {
        utils.fetchData(this.base_data_url("all_power_search"), (data) => {
          this.all_powers = data;
          this.processPowers();
          if (this.search_param) {
            this.initial_value = this.itemFromValue(this.search_param);
          }
          this.ready = true;
        });
      },
    },
    created() {
      // Attach onpopstate event handler
      window.onpopstate = (event)=> {
        const params = (new URL(window.location)).searchParams;
        const full_name = params.get("search");
        if (full_name === null) {
          this.search_param = "";
          document.title = "City of Data";
        }
        else {
          this.search_param = full_name;
          this.fetchData();
        }
      };
    },
    mounted() {
      const params = (new URL(window.location)).searchParams;
      const full_name = params.get("search");
      if (full_name) {
        this.search_param = full_name;
      }
      this.fetchData();
    },
    watch: {
    },
  };
};
