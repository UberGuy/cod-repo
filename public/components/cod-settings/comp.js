/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import * as settings from "/js/settings.js";
import { CONFIG } from "/config/config.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";


// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-settings");
  return {
    name: "cod-settings",
    template: template,
    mixins: [urls_mixin],
    components: {
    },
    data() {
      return {
        player_ats: [],
        display_100_pct: true,
        show_raw_data: false,
        show_pvp_and_pve: true,
        show_pvp_or_pve: false,
        always_prefer_at_none: false,
        preferred_default_at: 'none',
        data_path: CONFIG.data_path,
        location: window.location,
      };
    },
    props: {
      event_bus: {
        type: Vue,
        required: true,
      },
    },
    methods: {
      fetchData() {
        if (!this.player_ats.length) {
          utils.fetchData(this.at_index_url(), (data) => {
            if (data) {
              this.player_ats = ["none", ...data.player_archetypes];
            }
          });
        }
      },
      loadLocalStorage() {
        for (const field in settings.STORAGE_MAP) {
          this[field] = settings.get_setting(field, this[field]);
        }
      },
      display_name(name) {
        return utils.capitalize_words(name.replace(/_|:/g, " "));
      },
    },
    computed: {
    },
    watch: {
      data_path: function(value, oldval) {
        if (this.event_bus) {
          this.$nextTick(() => {
            this.event_bus.$emit("update-data-path", value);
          });
        }
      },
    },
    created() {
      this.fetchData();
      for (const field in settings.STORAGE_MAP) {
        this.$watch(field, (value) => settings.set_setting(field, value));
      }
    },
    mounted() {
      this.loadLocalStorage();
    },
  };
};
