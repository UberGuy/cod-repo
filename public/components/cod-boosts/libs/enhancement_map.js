/*jshint esversion: 6 */

// Maps enhancement boost names to icon names.
export const ENHANCEMENT_MAP = new Map();

// Standard boosts
ENHANCEMENT_MAP.set("Enhance Accuracy", "accuracy_boost.png");
ENHANCEMENT_MAP.set("Enhance Defense", "defense_boost.png");
ENHANCEMENT_MAP.set("Enhance ToHit Buffs", "tohit_boost.png");
ENHANCEMENT_MAP.set("Enhance Confuse", "confuse_boost.png");
ENHANCEMENT_MAP.set("Enhance Damage", "damage_boost.png");
ENHANCEMENT_MAP.set("Enhance Damage Resistance", "resist_damage_boost.png");
ENHANCEMENT_MAP.set("Enhance Defense DeBuff", "debuff_defense_boost.png");
ENHANCEMENT_MAP.set("Enhance ToHit DeBuffs", "debuff_tohit_boost.png");
ENHANCEMENT_MAP.set("Enhance Fear", "fear_boost.png");
ENHANCEMENT_MAP.set("Enhance Flying Speed", "fly_speed_boost.png");
ENHANCEMENT_MAP.set("Enhance Heal", "heal_boost.png");
ENHANCEMENT_MAP.set("Enhance Immobilization", "immobilize_boost.png");
ENHANCEMENT_MAP.set("Enhance Jump", "jump_boost.png");
ENHANCEMENT_MAP.set("Enhance KnockBack", "knockback_boost.png");
ENHANCEMENT_MAP.set("Enhance Recharge Speed", "recharge_time_boost.png");
ENHANCEMENT_MAP.set("Enhance Running Speed", "run_speed_boost.png");
ENHANCEMENT_MAP.set("Enhance Sleep", "sleep_boost.png");
ENHANCEMENT_MAP.set("Enhance Disorient", "stun_boost.png");
ENHANCEMENT_MAP.set("Enhance Range", "range_boost.png");
ENHANCEMENT_MAP.set("Reduce Endurance Cost", "endurance_discount_boost.png");
ENHANCEMENT_MAP.set("Enhance Damage Buffs", "damage_boost.png");
ENHANCEMENT_MAP.set("Enhance Taunt", "taunt_boost.png");
ENHANCEMENT_MAP.set("Enhance Slow Movement", "slow_boost.png");
ENHANCEMENT_MAP.set("Enhance Hold", "hold_boost.png");
ENHANCEMENT_MAP.set("Reduce Interrupt Time", "interrupt_reduction_boost.png");
ENHANCEMENT_MAP.set("Enhance Endurance Modification", "endurance_modification_boost.png");

// Set categories
ENHANCEMENT_MAP.set("Accurate Defense Debuff", "accurate_debuff_defense_sets.png");
ENHANCEMENT_MAP.set("Accurate Healing", "accurate_heal_sets.png");
ENHANCEMENT_MAP.set("Accurate To-Hit Debuff", "accurate_debuff_tohit_sets.png");
ENHANCEMENT_MAP.set("Blaster Archetype Sets", "blaster_at_sets.png");
ENHANCEMENT_MAP.set("Brute Archetype Sets", "brute_at_sets.png");
ENHANCEMENT_MAP.set("Confuse", "confuse_sets.png");
ENHANCEMENT_MAP.set("Controller Archetype Sets", "controller_at_sets.png");
ENHANCEMENT_MAP.set("Corruptor Archetype Sets", "corruptor_at_sets.png");
ENHANCEMENT_MAP.set("Defender Archetype Sets", "defender_at_sets.png");
ENHANCEMENT_MAP.set("Defense Debuff", "debuff_defense_sets.png");
ENHANCEMENT_MAP.set("Defense Sets", "defense_sets.png");
ENHANCEMENT_MAP.set("Dominator Archetype Sets", "dominator_at_sets.png");
ENHANCEMENT_MAP.set("Endurance Modification", "endurance_modification_sets.png");
ENHANCEMENT_MAP.set("Fear", "fear_sets.png");
ENHANCEMENT_MAP.set("Flight", "flight_sets.png");
ENHANCEMENT_MAP.set("Healing", "heal_sets.png");
ENHANCEMENT_MAP.set("Hold", "hold_sets.png");
ENHANCEMENT_MAP.set("Immobilize", "immobilize_sets.png");
ENHANCEMENT_MAP.set("Kheldian Archetype Sets", "kheldian_at_sets.png");
ENHANCEMENT_MAP.set("Knockback", "knockback_sets.png");
ENHANCEMENT_MAP.set("Leaping", "leaping_sets.png");
ENHANCEMENT_MAP.set("Mastermind Archetype Sets", "mastermind_at_sets.png");
ENHANCEMENT_MAP.set("Melee Damage", "melee_damage_sets.png");
ENHANCEMENT_MAP.set("Melee AoE Damage", "pbaoe_damage_sets.png");
ENHANCEMENT_MAP.set("Pet Damage", "pet_damage_sets.png");
ENHANCEMENT_MAP.set("Ranged Damage", "ranged_damage_sets.png");
ENHANCEMENT_MAP.set("Ranged AoE Damage", "taoe_damage_sets.png");
ENHANCEMENT_MAP.set("Recharge Intensive Pets", "recharge_intensive_pet_sets.png");
ENHANCEMENT_MAP.set("Resist Damage", "resist_damage_sets.png");
ENHANCEMENT_MAP.set("Running", "running_sets.png");
ENHANCEMENT_MAP.set("Scrapper Archetype Sets", "scrapper_at_sets.png");
ENHANCEMENT_MAP.set("Sentinel Archetype Sets", "sentinel_at_sets.png");
ENHANCEMENT_MAP.set("Sleep", "sleep_sets.png");
ENHANCEMENT_MAP.set("Slow Movement", "slow_sets.png");
ENHANCEMENT_MAP.set("Sniper Attack", "sniper_sets.png");
ENHANCEMENT_MAP.set("Soldiers of Arachnos Archetype Sets", "soa_at_sets.png");
ENHANCEMENT_MAP.set("Stalker Archetype Sets", "stalker_at_sets.png");
ENHANCEMENT_MAP.set("Stun", "stun_sets.png");
ENHANCEMENT_MAP.set("Tanker Archetype Sets", "tanker_at_sets.png");
ENHANCEMENT_MAP.set("Taunt", "taunt_sets.png");
ENHANCEMENT_MAP.set("Teleport", "teleport_sets.png");
ENHANCEMENT_MAP.set("To Hit Buff", "tohit_sets.png");
ENHANCEMENT_MAP.set("To Hit Debuff", "debuff_tohit_sets.png");
ENHANCEMENT_MAP.set("Universal Damage Sets", "universal_damage_sets.png");
ENHANCEMENT_MAP.set("Universal Travel", "universal_travel_sets.png");
