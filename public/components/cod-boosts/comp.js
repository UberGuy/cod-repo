/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

import { ENHANCEMENT_MAP } from "/components/cod-boosts/libs/enhancement_map.js";

// Change weird plurals
const BOOSTSET_NAME_REMAP = {
  "Holds": "Hold",
  "Stuns": "Stun",
  "Sniper Attacks": "Sniper Attack",
};

const BOOSTS_TO_IGNORE = new Set([
  "Magic_Boost",
  "Mutation_Boost",
  "Natural_Boost",
  "Science_Boost",
  "Technology_Boost"
]);

const EFFECT_TYPES_TO_HIDE_BOOSTS_FOR = new Set([
  "Enhancement",
  "Global Enhancement",
]);

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-boosts");
  return {
    name: "cod-boosts-allowed",
    template: template,
    mixins: [urls_mixin],
    data() {
      return { };
    },
    props: [
      "power_data",
    ],
    methods: {
      get_icon_name(boost_or_boostcat) {
        return ENHANCEMENT_MAP.get(boost_or_boostcat);
      },
      boost_icon_path(boost) {
        return this.boost_icon_url(this.get_icon_name(boost));
      },
      boostset_icon_path(boostset) {
        return this.boostset_icon_url(this.get_icon_name(boostset));
      },
    },
    computed: {
      boosts_allowed() {
        for (const boost of this.power_data.boosts_allowed) {
          if (!ENHANCEMENT_MAP.has(boost) && !BOOSTS_TO_IGNORE.has(boost)) {
            console.log(`Unrecognized boost type '${boost}'`);
          }
        }
        return this.power_data.boosts_allowed
          .slice().sort().filter(
            x => !BOOSTS_TO_IGNORE.has(x) && ENHANCEMENT_MAP.has(x)
          );
      },
      allowed_boostset_cats() {
        let to_sort = [];
        let all_sets = this.power_data.allowed_boostset_cats;
        let at_sets = all_sets.filter(s => s.endsWith("Archetype Sets")).sort();
        let uni_dmg = all_sets.filter(s => s === "Universal Damage Sets");
        let partial = uni_dmg.concat(at_sets);
        let already_seen = new Set(partial);
        for (const s of all_sets) {
          if (already_seen.has(s)) continue;
          to_sort.push(BOOSTSET_NAME_REMAP[s] || s);
        }
        return to_sort.sort().concat(partial);
      },
      show_allowed_boosts() {
        // return !EFFECT_TYPES_TO_HIDE_BOOSTS_FOR.has(this.power_data.type);
        return true;
      },
    },
  };
};
