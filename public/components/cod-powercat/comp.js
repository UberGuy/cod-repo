/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";
import tippy_mixin from "/components/cod-tippy/libs/tippy_mixin.js";

function setTitle(powercat_data) {

  let pcat_name = powercat_data.name.replace(/_/g, " ");
  document.title = `Category '${pcat_name}' (${powercat_data.name})`;
}

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-powercat");
  return {
    name: "cod-powercat",
    template: template,
    mixins: [urls_mixin, tippy_mixin],
    components: {
    },
    data() {
      return {
        powercat_data: null,
        powercat_path: "",
        show_raw_data: false,
      };
    },
    props: {
      event_bus: {
        type: Vue,
        required: false,
      },
    },
    methods: {
      setHistory() {
        const current_loc = window.location.pathname + window.location.search;
        const to_push = this.powercat_url(this.powercat_path);
        // If we're already at this address (such as by direct link)
        // then don't push it onto the history stack again
        if (current_loc !== to_push) {
          // If we're redirecting to a lower-cased version of the
          // same path, just overwrite the location
          if (current_loc.toLowerCase() === to_push) {
            window.history.replaceState(this.powerset_path, null, to_push);
          }
          else {
            window.history.pushState(this.powerset_path, null, to_push);
          }
        }
      },
      fetchData({pushstate=true} = {}) {
        if (this.powercat_path !== null) {
          this.powercat_path = this.powercat_path.toLowerCase();
          let pcat;
          [pcat, ] = this.powercat_path.split(".");
          if (pushstate) {
            this.setHistory();
          }
          utils.fetchData(this.pcat_data_url(pcat), (response) => {
            this.powercat_data = response;
            if (response) {
              setTitle(response);
            }
          });
        }
      },
    },
    computed: {
      sorted_powersets() {

        const data = this.powercat_data;
        if (!data) return [];

        const sorted_sets = [];
        for (const [index, fullName] of data.powerset_names.entries()) {
          let displayName = data.powerset_display_names[index];
          const lcFullName = fullName.toLowerCase();
          const internalName = fullName.split(".")[1];
          // Hack to deal with ambiguous display names of some newer sets
          if (lcFullName.startsWith("boosts")) {
            if (lcFullName.includes("attuned") && !displayName.includes("Attuned")) {
              displayName = `Attuned ${displayName}`;
            }
            if (lcFullName.includes("crafted") && !displayName.includes("Crafted")) {
              displayName = `Crafted ${displayName}`;
            }
          }
          const entry = {
            fullName,
            displayName: (displayName ? displayName : internalName).replace(/_/g, " "),
            index,
          };
          sorted_sets.push(entry);
        }
        sorted_sets.sort((a, b) => {
          if (a.displayName > b.displayName) return 1;
          if (a.displayName < b.displayName) return -1;
          return 0;
        });
        return sorted_sets;
      },
      powerset_dupes() {
        let dupes = new Map();
        for (const powerset_obj of this.sorted_powersets) {
          const key = powerset_obj.displayName;
          dupes.set(key, (dupes.get(key) || 0) + 1);
        }
        return dupes;
      },
      // Build HTML for use in the Tippy "links" icon
      archetype_links() {
        if (!this.powercat_data) return "";

        let links = [];
        const archetypes = (this.powercat_data.archetypes || []).sort();
        for (const at of archetypes) {
          const display_name = utils.snake_to_ucamel_case(at);
          links.push(
            `<a href="${this.at_url(at)}">${display_name}</a>`
          );
        }
        return `<div class=title-tip>Archetypes<br>${links.join("<br>")}</div>`;
      },
    },
    watch: {
    },
    created() {
      // Attach onpopstate event handler
      window.onpopstate = (event)=> {
        const params = (new URL(window.location)).searchParams;
        const full_name = params.get("pcat");
        if (full_name === null) {
          this.powercat_path = "";
          this.powercat_data = null;
          document.title = "City of Data";
        }
        else {
          this.powercat_path = full_name;
          this.input_key = !this.input_key;
          this.fetchData({pushstate:false});
        }
      };
    },
    mounted() {
      const params = (new URL(window.location)).searchParams;
      const full_name = params.get("pcat");
      if (full_name) {
        this.powercat_path = full_name;
        this.fetchData();
        if (this.event_bus) {
          // Set up handler for checkbox update events
          this.event_bus.$on("update-checkbox", ([checkbox, value]) => {
            Vue.set(this, checkbox, value);
          });
          // Send event requesting the current checkbox settings (as events)
          setTimeout(
            () => {
              this.event_bus.$emit("request-checkbox-settings");
            }, 5
          );
        }
      }
    }
  };
};
