/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import * as settings from "/js/settings.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";
import tippy_mixin from "/components/cod-tippy/libs/tippy_mixin.js";

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-header");
  return {
    name: "cod-header",
    template: template,
    mixins: [urls_mixin, tippy_mixin],
    data() {
      return {
        revision: null,
        updating: false,
        key: 0,
      };
    },
    props: {
      event_bus: {
        type: Object,
        default: null,
      },
    },
    methods: {
      fetchData() {
        if (!this.revision) {
          utils.fetchData(this.base_data_url("index"), (data) => {
            this.revision = data.revision;
            if (this.updating) {
              this.$nextTick(() => this.updating = false);
              console.log(`Switched to data revision ${this.revision}`);
            }
            this.$nextTick(() => ++this.key);
          });
        }
      },
      toCryptic() {
        settings.set_setting("data_path", "/cryptic");
        location.reload();
      },
      toHomecoming() {
        settings.set_setting("data_path", "/homecoming");
        location.reload();
      },
      pop() {
        if (this.showHomecoming || this.showCryptic) {
          const popup = document.getElementById("popup");
          popup.classList.remove("hide");
          popup.classList.add("show");
        }
      },
      unpop() {
        const popup = document.getElementById("popup");
        popup.classList.remove("show");
        popup.classList.add("hide");
      },
    },
    computed: {
      showCryptic() {
        return (
          ['cod.cohcb.com', 'cod.net.uber'].includes(location.hostname) &&
          this.revision &&
          !this.revision.startsWith('cryptic')
        );
      },
      showHomecoming() {
        return (
          this.revision &&
          !this.revision.startsWith('homecoming')
        );
      },
    },
    mounted() {
      // Load the revision string
      this.fetchData();
      if (this.event_bus) {
        // Set up handler for data path update events
        this.event_bus.$on("update-data-path", (value) => {
          this.revision = null;
          this.updating = true;
          this.fetchData();
        });
      }
    },
    watch: {
    }
  };
};
