/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-help");
  return {
    name: "cod-help",
    template: template,
    mixins: [urls_mixin],
    components: {
    },
    data() {
      return {
        current_page: null,
      };
    },
    props: {
    },
    methods: {
      setHistory() {
        const current_loc = window.location.pathname + window.location.search;
        const to_push = this.help_url(this.current_page);
        // If we're already at this address (such as by direct link)
        // then don't push it onto the history stack again
        if (current_loc !== to_push) {
          const params = (new URL(window.location)).searchParams;
          // If we're redirecting to a lower-cased version of the
          // same path, just overwrite the location
          if (current_loc.toLowerCase() === to_push || !params.get("page")) {
            window.history.replaceState(this.current_page, null, to_push);
          }
          else {
            window.history.pushState(this.current_page, null, to_push);
          }
        }
      },
      onClick(event) {
        this.current_page = event.target.id;
        this.setHistory();
      },
      isCurrent(item) {
        return (this.current_page === item);
      },
    },
    computed: {
      help_items() {
        return [
          ["overview", "Overview"],
          ["main-page", "Main Page"],
          ["archetype-page", "Archetype Page"],
          ["powercat-page", "Power Category Page"],
          ["powerset-page", "Powerset Page"],
          ["power-page", "Power Page"],
          ["modifiers-page", "Modifiers Page"],
          ["attributes-page", "Attributes Page"],
        ];
      },
      help_pages() {
        const page_items = [];
        for (const [help_item, title] of this.help_items) {
          page_items.push({
            item: help_item,
            url: this.help_url_raw(help_item),
            name: title,
          });
        }
        return page_items;
      },
    },
    watch: {
    },
    created() {
      // Attach onpopstate event handler
      window.onpopstate = (event) => {
        const params = (new URL(window.location)).searchParams;
        const query_page = params.get("page");
        if (query_page !== null) {
          this.current_page = query_page;
        }
      };
      if (!this.query_page) {
        this.current_page = this.help_items[0][0];
      }
    },
    mounted() {
      const params = (new URL(window.location)).searchParams;
      const query_page = params.get("page");
      if (query_page !== null) {
        this.current_page = query_page;
      }
      this.setHistory();
    }
  };
};
