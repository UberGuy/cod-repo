/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

function setTitle(group_name) {

  if (group_name) {
    document.title = `Boostset Group '${group_name}'`;
  }
  else {
    document.title = "Boostset Groups";
  }
}

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-boostset-groups");
  return {
    name: "cod-boostset-groups",
    template: template,
    mixins: [urls_mixin],
    components: {
    },
    data() {
      return {
        all_groups: null,
        group_name: null,
      };
    },
    props: {
    },
    methods: {
      fetchData({pushstate=true} = {}) {
        if (this.group_name !== null) {
          this.group_name = this.group_name.toLowerCase();
          if (pushstate) {
            const params = (new URL(window.location)).searchParams;
            const group_name = params.get("group");
            const to_push = this.boostset_group_url(this.group_name);
            // If we're already at this address (such as by direct link)
            // then don't push it onto the history stack again
            if (group_name.toLowerCase() === this.group_name) {
              window.history.replaceState(this.group_name, null, to_push);
            }
            else {
              window.history.pushState(this.group_name, null, to_push);
            }
          }
        }

        utils.fetchData(this.base_data_url("boostset_groups"), (data) => {
          this.all_groups = data;
          setTitle();
          this.ready = true;
        });
      },
      group_url(group) {
        return this.boostset_group_url(group);
      },
    },
    computed: {
      processed_groups() {
        const pg = {};
        for (const [name, sets] of Object.entries(this.all_groups)) {
          console.log(name);
          pg[name.toLowerCase().replace(/\s+/g, "_")] = {
            name: name.replace("Archetype Sets", "Sets"),
            sets: sets,
          };
        }
        return pg;
      },
      group_list() {
        return this.all_groups ? [...Object.keys(this.processed_groups)] : [];
      },
      group_display_name() {
        if (!this.group_name) {
          return null;
        }
        return this.processed_groups[this.group_name].name;
      },
      group_members() {
        return [...this.processed_groups[this.group_name].sets];
      },
    },
    watch: {
    },
    created() {
      // Attach onpopstate event handler
      window.onpopstate = (event)=> {
        const params = (new URL(window.location)).searchParams;
        const group_name = params.get("group");
        if (group_name === null) {
          document.title = "City of Data";
        }
        else {
          this.group_name = group_name;
        }
        this.fetchData({pushstate:false});
      };
    },
    mounted() {
      const params = (new URL(window.location)).searchParams;
      const group_name = params.get("group");
      if (group_name !== null) {
        this.group_name = group_name;
      }
      this.fetchData();
    }
  };
};
