/*jshint esversion: 8 */

import * as utils from "/js/utils.js";

import urls_mixin from "/components/cod-links/libs/urls_mixin.js";
import tippy_mixin from "/components/cod-tippy/libs/tippy_mixin.js";

const BOOST_POGS = new Map();

BOOST_POGS.set("Enhance Accuracy", "e_pog_accuracy.png");
BOOST_POGS.set("Enhance Defense", "e_pog_buff_defense.png");
BOOST_POGS.set("Enhance ToHit Buffs", "e_pog_buff_to_hit.png");
BOOST_POGS.set("Enhance Confuse", "e_pog_confusion_duration.png");
BOOST_POGS.set("Enhance Damage", "e_pog_damage.png");
BOOST_POGS.set("Enhance Damage Resistance", "e_pog_buff_damage_resist.png");
BOOST_POGS.set("Enhance Defense DeBuff", "e_pog_debuff_defense.png");
BOOST_POGS.set("Enhance ToHit DeBuffs", "e_pog_debuff_to_hit.png");
BOOST_POGS.set("Enhance Fear", "e_pog_fear_duration.png");
BOOST_POGS.set("Enhance Flying Speed", "e_pog_fly_speed.png");
BOOST_POGS.set("Enhance Heal", "e_pog_heal.png");
BOOST_POGS.set("Enhance Immobilization", "e_pog_immobilization_duration.png");
BOOST_POGS.set("Enhance Jump", "e_pog_jump_distance.png");
BOOST_POGS.set("Enhance KnockBack", "e_pog_knockback_distance.png");
BOOST_POGS.set("Enhance Recharge Speed", "e_pog_recharge_time.png");
BOOST_POGS.set("Enhance Running Speed", "e_pog_run_speed.png");
BOOST_POGS.set("Enhance Sleep", "e_pog_sleep_duration.png");
BOOST_POGS.set("Enhance Disorient", "e_pog_stun_duration.png");
BOOST_POGS.set("Enhance Range", "e_pog_range_increase.png");
BOOST_POGS.set("Reduce Endurance Cost", "e_pog_end_discount.png");
BOOST_POGS.set("Enhance Damage Buffs", "damage_boost.png");
BOOST_POGS.set("Enhance Taunt", "e_pog_taunt_duration.png");
BOOST_POGS.set("Enhance Slow Movement", "e_pog_slow_movement.png");
BOOST_POGS.set("Enhance Hold", "e_pog_hold_duration.png");
BOOST_POGS.set("Reduce Interrupt Time", "e_pog_interrupt_times.png");
BOOST_POGS.set("Enhance Endurance Modification", "e_pog_recovery.png");

function setTitle(boostset_data) {

  document.title = `Boost Set '${boostset_data.display_name}' (${boostset_data.name})`;
}

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-boostset");
  return {
    name: "cod-boostset",
    template: template,
    mixins: [urls_mixin, tippy_mixin],
    components: {
    },
    data() {
      return {
        boostset_path: "",
        boostset_data: null,
        show_raw_data: false,
        input_key: false,
      };
    },
    props: {
      event_bus: {
        type: Vue,
        required: false,
      },
    },
    methods: {
      setHistory() {
        const params = (new URL(window.location)).searchParams;
        const set_name = params.get("set");
        let to_push = this.boostset_url(this.boostset_path);
        // If we're already at this address (such as by direct link)
        // then don't push it onto the history stack again
        if (this.boostset_path !== set_name) {
          // If we're redirecting to a lower-cased version of the
          // same path, just overwrite the location.
          if (set_name && set_name.toLowerCase() === this.boostset_path) {
            window.history.replaceState(this.boostset_path, null, to_push);
          }
          else {
            window.history.pushState(this.boostset_path, null, to_push);
          }
        }
      },
      fetchData({pushstate=true} = {}) {
        if (this.boostset_path !== null) {
          this.boostset_path = this.boostset_path.toLowerCase();
          if (pushstate) {
            this.setHistory();
          }
          utils.fetchData(this.boostset_data_url(this.boostset_path), (response) => {
            this.boostset_data = response;
            if (this.boostset_data) {
              setTitle(this.boostset_data);
            }
          });
        }
      },
      global_for_power(power_name) {
        const fullanme = this.fullnames_by_powername[power_name];
        if (power_name) {
          return this.boostset_data.computed.globals[power_name];
        }
        return null;
      },
      loadLocalStorage() {
      },
    },
    computed: {
      boost_lists() {
        const lists = [];
        for (const [idx, boost_list] of this.boostset_data.boost_lists.entries()) {
          const list = [];
          for (const [idy, boost] of boost_list.entries()) {
            list.push({
              descriptive_name: this.boostset_data.computed.boosts[idx][idy],
              power_fullname: boost.toLowerCase(),
            });
          }
          lists.push(list);
        }
        return lists;
      },
      globals_by_piece() {
        const globals = [];
        for (const boost_list of this.boostset_data.boost_lists) {
          const piece_globals = {};
          for (const boost of boost_list) {
            const power_name = boost.toLowerCase().split(".")[2];
            if (power_name) {
              const bonuses = this.boostset_data.computed.globals[power_name];
              if (bonuses) {
                bonuses.forEach(e => piece_globals[e[0]] = e[1]);
              }
            }
          }
          globals.push(piece_globals);
        }
        return globals;
      },
      boost_icon() {
        return this.base_icon_url(this.boostset_data.computed.icons[0]);
      },
      boost_pog() {
        const boost_type = this.boostset_data.computed.boost_type[0];
        return this.base_icon_url(BOOST_POGS.get(boost_type));
      },
      boost_icon_borders() {
        const borders = [];
        const boost_list = this.boostset_data.boost_lists[0];
        for (const boost of boost_list) {
          if (boost.includes("Attuned")) {
            if (boost.includes("Superior")) {
              borders.push([
                "e_origin_superiorattune_l.png",
                "e_origin_superiorattune_r.png"
              ]);
            }
            else {
              borders.push([
                "e_origin_attune_l.png",
                "e_origin_attune_r.png"
              ]);
            }
          }
          else {
            borders.push([
              "e_orgin_invention_l.png",
              "e_orgin_invention_r.png"
            ]);
          }
        }
        return borders.map(x => x.map(y => this.base_icon_url(y)));
      }
    },
    watch: {
    },
    created() {
      // Attach onpopstate event handler
      window.onpopstate = (event)=> {
        const params = (new URL(window.location)).searchParams;

        const boostset_name = params.get("set");
        if (boostset_name == null) {
          this.boostset_path = "";
          this.boostset_data = null;
          this.input_key = !this.input_key;
          document.title = "City of Data";
        }
        else {
          this.power_path = boostset_name;
          this.input_key = !this.input_key;
          this.fetchData({pushstate:false});
        }
      };
    },
    mounted() {
      this.loadLocalStorage();
      const params = (new URL(window.location)).searchParams;
      const boostset_name = params.get("set");
      if (boostset_name) {
        this.boostset_path = boostset_name;
        this.fetchData();
        if (this.event_bus) {
          // Set up handler for checkbox update events
          this.event_bus.$on("update-checkbox", ([checkbox, value]) => {
            Vue.set(this, checkbox, value);
          });
          // Send event requesting the current checkbox settings (as events)
          setTimeout(
            () => {
              this.event_bus.$emit("request-checkbox-settings");
            }, 5
          );
        }
      }
    }
  };
};
