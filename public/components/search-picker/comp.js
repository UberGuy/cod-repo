/*jshint esversion: 8 */

import { CONFIG } from "/config/config.js";
import * as utils from "/js/utils.js";

// Return a Promise so we can retrieve the template externally
export default async () => {
  const template = await utils.fetch_template("search-picker");
  return {
    name: "search-picker",
    template: template,
    data () {
      return {
        inputValue: "",
        selectedItem: {},
        hoverItem: -1,
        picking: false,
        hover: false,
        moreItemsThanShown: false,
        pressTimer: null,
      };
    },
    mounted () {
      if (this.initialItem) {
        this.selectItem(this.initialItem);
      }
      this.$emit("picker-ready");
    },
    props: {
      itemList: {
        type: Array,
        required: true,
      },
      initialItem: {
        type: Object,
        required: false,
      },
      maxItems: {
        type: Number,
        default: 20,
      },
      placeholder: {
        type: String,
        required: false,
      },
    },
    methods: {
      startSelection() {
        // this.selectedItem = {};
        this.$refs.dropdowninput.focus();
        this.hoverItem = 0;
        this.showInput();
        this.$nextTick(() => this.$refs.dropdowninput.focus());
        // this.picking = false;
        // this.$emit('on-item-reset');
      },
      selectItem(theItem) {
        this.selectedItem = theItem;
        this.resetState();
      },
      itemVisible(item) {
        if (this.inputValue.length === 0) {
          return true;
        }
        if (this.inputValue.includes("*")) {
          let patt = this.inputValue.replace(/[-\/\\^$+?.()|[\]{}]/g, '\\$&');
          patt = patt.replace(/[*]+/g, ".*");
          const regex = new RegExp(String.raw`(${patt})`, 'gi');
          return regex.test(item.option);
        }
        let currentName = item.option.toLowerCase();
        let currentInput = this.inputValue.toLowerCase();
        return currentName.includes(currentInput);
      },
      getHtml(item) {
        let baseHtml = `<span>${this.highlight(item)}</span>`;
        if (this.visibleDuplicates.get(item.option) > 1) {
          baseHtml += `<span style="font-size:90%">${item.value}</span>`;
        }
        return baseHtml;
      },
      highlight(item) {
        let highlighted;
        if (this.inputValue.length > 0) {
          let patt = this.inputValue.replace(/[-\/\\^$+?.()|[\]{}]/g, '\\$&');
          if (this.inputValue.includes("*")) {
            patt = patt.replace(/[*]+/g, ".*");
          }
          const regex = new RegExp(String.raw`(${patt})`, 'gi');
          let replaced = item.option.replace(
            regex,
            "<span class=search-highlight>$1</span>"
          );
          return replaced.replace(/>\s/g, ">&nbsp;").replace(/\s</g, "&nbsp;<");
        }
        return item.option;
      },
      stopPicking() {
        if (!this.hover) {
          this.resetState();
        }
      },
      doHover(index) {
        this.hoverItem=index;
      },
      clear() {
        this.resetState();
      },
      showInput() {
        this.$refs.dropdowninput.style.display = null;
        this.$refs.dropdownselected.style.display = "none";
      },
      showSelected() {
        if (this.selectedItem) {
          if (this.selectedItem.value !== undefined) {
            this.$refs.dropdowninput.style.display = "none";
            this.$refs.dropdownselected.style.display = null;
          }
        }
      },
      onInput: utils.debounce(function($event) {
        this.inputValue = $event.target.value.trim();
      }, 200),

      onEnter() {
        if (this.visibleItems.length === 0) {
          return;
        }
        if (this.visibleItems.length === 1) {
          this.selectItem(this.visibleItems[0]);
        }
        else {
          if (this.hoverItem >= 0) {
            this.selectItem(this.visibleItems[this.hoverItem]);
          }
        }
      },
      resetState() {
        this.picking=false;
        this.hoverItem = 0;
        this.inputValue = "";
        this.pressTimer = null;
        this.hoverItem = 0;
        this.$refs.dropdowninput.value = "";
        this.showSelected();
        this.$nextTick(() => this.$refs.dropdowninput.blur());
      },
      decrementIndex() {
        if (this.hoverItem > 0) this.hoverItem--;
        this.cancelTimer();
        this.setTimer(() => this.decrementIndex());
      },
      incrementIndex() {
        if (this.hoverItem < (this.visibleItems.length) - 1) this.hoverItem++;
        this.setTimer(() => this.incrementIndex());
      },
      setTimer(func) {
        this.cancelTimer();
        this.pressTimer = setTimeout(func, 750);
      },
      cancelTimer() {
        if (this.pressTimer !== null) {
          clearTimeout(this.pressTimer);
          this.pressTimer = null;
        }
      },
    },
    computed: {
      visibleItems() {
        let items = [];
        let overflowed = false;
        for (const item of this.itemList) {
          if (items.length >= this.maxItems) {
            overflowed = true;
            break;
          }
          if (this.itemVisible(item)) {
            items.push(item);
          }
        }
        this.moreItemsThanShown = overflowed;
        return items;
      },
      visibleDuplicates() {
        let dupeMap = new Map();
        for (const item of this.visibleItems) {
          dupeMap.set(item.option, (dupeMap.get(item.option) || 0) + 1);
        }
        return dupeMap;
      }
    },
    watch: {
      selectedItem: function(value) {
        if (value !== undefined) {
          this.$emit('on-item-selected', value);
        }
      },
      hoverItem: function(newItem, oldItem) {
        // I probably should do this through v-bind, but
        // I couldn't figure out a way that worked.
        this.$nextTick(() => {
          // Grab the DOM references
          let oldRef = this.$refs[`dropdownitem_${oldItem}`];
          let newRef = this.$refs[`dropdownitem_${newItem}`];
          // Un-highlight the old item via CSS
          if (oldRef && oldRef.length) {
            oldRef[0].classList.remove("dropdown-item-selected");
          }
          // Highlight the new item via CSS
          if (newRef && newRef.length) {
            newRef[0].classList.add("dropdown-item-selected");
          }
        });
      },
      // picking: function(value) {
      //   console.log("Picking:",value);
      // }
    },
  };
};
