/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import { get_setting } from "/js/settings.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

import SearchPicker from "/components/search-picker/comp.js";

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-at-picker");
  return {
    name: "cod-at-picker",
    template: template,
    mixins: [urls_mixin],
    components: {
      "search-picker": SearchPicker,
    },
    data() {
      return {
        archetype: null,
        archetypeTables: new Map(),
        pc_archetypes: undefined,
        npc_archetypes: undefined,
        powers_by_at: undefined,
        ats_for_powercat:undefined,
        power_fullname: "",
        powercat: "",
        powerset: "",
        powername: "",
        ready: false,
      };
    },
    props: {
      freeform_classes: {
        type: Boolean,
        default: false,
      },
      event_bus: {
        type: Object,
        default: null,
      },
      power_data: {
        type: Object,
        required: true,
      },
      initial_at: {
        type: String,
        required: false,
      },
    },
    computed: {
      // ready() {
      //   return (
      //     this.pc_archetypes !== undefined &&
      //     this.powers_by_at !== undefined &&
      //     this.ats_for_powercat != undefined
      //   );
      // },
      all_archetypes() {
        const all_archetypes = [...this.pc_archetypes];
        all_archetypes.push(...this.npc_archetypes);
        return all_archetypes;
      },
      valid_archetypes() {
        if (this.freeform_classes) {
          return this.all_archetypes;
        }

        // All PC classes get access to all standard power pools
        if (this.powercat == "pool")
          return this.pc_archetypes;

        // Look up the AT by requirements expression mapping
        // It only contains PC archetypes
        let valid_for_at = [];
        for (const at of this.pc_archetypes) {
          let at_powers = this.powers_by_at.get(at);
          if (utils.checkArray(at_powers)) {
            for (const power_obj of at_powers) {
              if (power_obj[1] === this.power_fullname) {
                valid_for_at.push(at.toLowerCase());
              }
            }
          }
        }
        if (valid_for_at.length > 0) {
          return valid_for_at;
        }

        // Defer to the power if it lists ATs
        if (this.power_data.archetypes.length > 0) {
          return this.power_data.archetypes.map(at => at.toLowerCase());
        }

        // Defer to the powercat if it lists ATs
        if (this.ats_for_powercat.length > 0) {
          return this.ats_for_powercat.map(at => at.toLowerCase());
        }

        // And if all that failed to match, return all ATs,
        // including NPC ones, since this may be an NPC power
        return this.all_archetypes;
      },
      dropdown_data() {
        let options = [];
        options.push({
          value: null,
          option: "None"
        });
        for (const at of this.valid_archetypes) {
          options.push({
            value: at,
            option: utils.snake_to_ucamel_case(at),
          });
        }
        return options;
      },
    },
    methods: {
      setArchetype(picker_item) {
        console.log("Selecting", picker_item.value);
        this.archetype = picker_item.value;
      },
      // Meant to be called once after all data fetches are complete
      setInitialValue() {
        // Always prefer "None"?
        if (!!get_setting("always_prefer_at_none")) {
          this.initial_value = this.dropdown_data[0];
          return;
        }

        // Do we have an initial AT prop?
        if (this.initial_at) {
          // If we get explicit 'none' as the AT, pick that (item '0')
          if (this.initial_at === "none") {
            this.initial_value = this.dropdown_data[0];
          }
          else {
            if (!this.valid_archetypes.includes(this.initial_at)) {
              if (!this.$parent.freeform_classes) {
                // TODO: This is a code smell. Use an event instead
                this.$parent.freeform_classes = true;
                Vue.nextTick(this.setInitialValue);
              }
            }
            // Else, find the element for the named AT
            for (const option of this.dropdown_data) {
              if (option.value === this.initial_at) {
                this.initial_value = option;
                break;
              }
            }
          }
        }
        // If we got here with no selected AT
        if (!this.initial_value) {
          // Is there just one AT in the list? If so, pick that
          if (this.dropdown_data.length === 2) {
            this.initial_value = this.dropdown_data[1];
          }
          // Otherwise see if the site-wide default is valid
          else {
            const default_at = get_setting("preferred_default_at");
            if (this.valid_archetypes.includes(default_at)) {
              // Find the element for the named AT
              for (const option of this.dropdown_data) {
                if (option.value === default_at) {
                  this.initial_value = option;
                  break;
                }
              }
            }
            // Otherwise pick 'none' (item '0')
            else {
              this.initial_value = this.dropdown_data[0];
            }
          }
        }
      },
    },
    watch: {
      archetype: function(at_name) {
        // Null means we set the AT selection to None
        if (at_name === null) {
          this.$emit("selected-at-changed", undefined);
        }
        // Do we have tables for this AT?
        else if (!this.archetypeTables.has(at_name)) {
          // Fetch it
          const data_path = this.mod_table_url(at_name);
          console.log(`Fetching ${data_path}`);
          utils.fetchData(data_path, (data) => {
            let tables = data.named_tables;
            tables.$archetype = at_name;
            tables.base_perception = data.base_perception;
            this.archetypeTables.set(at_name, tables);
            // Only fire the event after the tables are loaded
            this.$emit("selected-at-changed", tables);
          });
        }
        else {
          // We already have the tables, so let the parent have them
          this.$emit("selected-at-changed", this.archetypeTables.get(at_name));
        }
      }
    },
    created() {
      this.power_fullname = this.power_data.full_name.toLowerCase();
      [this.powercat, this.powerset, this.powername] = this.power_fullname.split(".");
      const p1 = utils.fetchData(this.at_index_url(), (data) => {
        this.pc_archetypes = data.player_archetypes;
        this.npc_archetypes = data.npc_archetypes;
      });
      const p2 = utils.fetchData(this.base_data_url("powers_by_at"), (data) => {
        this.powers_by_at = new Map(Object.entries(data));
      });
      const p3 = utils.fetchData(this.pcat_data_url(this.powercat), (data) => {
        this.ats_for_powercat = data.archetypes;
      });
      Promise.all([p1, p2, p3]).then(() => {
        this.setInitialValue();
        this.ready = true;
      });
    },
    mounted() {
    }
  };
};
