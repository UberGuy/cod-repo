/*jshint esversion: 8 */

import {fetch_template} from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";
import tippy_mixin from "/components/cod-tippy/libs/tippy_mixin.js";

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await fetch_template("cod-power-icons");
  return {
    name: "power-icons",
    template: template,
    mixins: [urls_mixin, tippy_mixin],
    props: {
      power_data: {
        type: Object,
        required: true,
      },
    },
    data() {
      return {
        ready: false,
      };
    },
    methods: {
    },
    computed: {
      needsLineOfSight() {
        if (this.ready && this.power_data.target_visibility === "LineOfSight") {
          return this.$refs.needsLineOfSight.innerHTML;
        }
        return false;
      },
      numberAllowed() {
        if (this.ready && this.power_data.number_allowed > 1) {
          return this.$refs.numberAllowed.innerHTML;
        }
        return false;
      },
      castThrough() {
        if (this.ready && this.power_data.cast_through.length) {
          return this.$refs.castThrough.innerHTML;
        }
        return false;
      },
      rechargeGroups() {
        if (this.ready && this.power_data.recharge_groups.length) {
          return this.$refs.rechargeGroups.innerHTML;
        }
        return false;
      },
      toggleDetoggleAfter() {
        if (this.ready && this.power_data.toggle_detoggle_time > 0) {
          return this.$refs.toggleDetoggleAfter.innerHTML;
        }
        return false;
      },
      toggleIgnores() {
        if (this.ready && this.power_data.toggle_ignores.length) {
          return this.$refs.toggleIgnores.innerHTML;
        }
        return false;
      },
      ignoresStrength() {
        if (this.ready && this.power_data.ignore_strength) {
          return this.$refs.ignoresStrength.innerHTML;
        }
      },
      strengthsDisallowed() {
        if (this.ready && this.power_data.strengths_disallowed.length) {
          return this.$refs.strengthsDisallowed.innerHTML;
        }
        return false;
      },
      globalStrengthsDisallowed() {
        if (this.ready && this.power_data.global_strengths_disallowed.length) {
          return this.$refs.globalStrengthsDisallowed.innerHTML;
        }
        return false;
      },
      modesDisallowed() {
        if (this.ready && this.power_data.modes_disallowed.length) {
          return this.$refs.modesDisllowed.innerHTML;
        }
        return false;
      },
      modesRequired() {
        if (this.ready && this.power_data.modes_required.length) {
          return this.$refs.modesRequired.innerHTML;
        }
        return false;
      },
      exclusionGroups() {
        if (this.ready && this.power_data.exclusion_groups.length) {
          return this.$refs.exclusionGroups.innerHTML;
        }
        return false;
      },
    },
    mounted() {
      this.$nextTick(function () {
        this.ready = true;
      });
    },
  };
};
