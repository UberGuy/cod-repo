/*jshint esversion: 8 */

import "/js/storage_shim.js";

export default {

  data() {
    return {
      checked: false,
    };
  },
  props: {
    name: {
      type: String,
      required: true,
    },
    id: {
      type: String,
      required: false,
    },
    event_bus: {
      type: Object,
      default: null,
    },
  },
  methods: {
    on_update(name, value, force) {
      const val = !!value;
      if (val !== localStorage.getObject(name)) {
        // Also update localstorage
        localStorage.setObject(name, val);
        this.event_bus.$emit("update-checkbox", [name, val]);
      }
      else if (!!force) {
        // Emit the event anyway
        this.event_bus.$emit("update-checkbox", [name, val]);
      }
    },
  },
  mounted() {
    this.checked = !!localStorage.getObject(this.name);

    // Register an event listener to handle requests to emit the current
    // status of the checkbox
    this.event_bus.$on("request-checkbox-settings", () => {
      this.on_update(this.name, this.checked, true);
    });
  },
  watch: {
    checked: function(value) {
      this.on_update(this.name, value);
    },
  }
};
