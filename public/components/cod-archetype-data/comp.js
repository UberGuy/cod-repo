/*jshint esversion: 8 */

import "/js/number_shim.js";
import * as utils from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

const VueSlider = window['vue-slider-component'];

const LEVEL_STORAGE_NAME = "power-display-level";

function setTitle(at_data) {

  document.title = `Archetype '${at_data.display_name}' (${at_data.name})`;
}

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-archetype-data");
  return {
    name: "cod-archetype-data",
    template: template,
    mixins: [urls_mixin],
    components: {
      "vue-slider": VueSlider,
    },
    data() {
      return {
        pc_ats: [],
        npc_ats: [],
        at_data: null,
        display_names: null,
        class_key: "blaster",
        display_level: 50,
        show_raw_data: false,
        show_descriptions: false,
      };
    },
    props: {
      event_bus: {
        type: Vue,
        required: true,
      },
    },
    methods: {
      setHistory() {
        const current_loc = window.location.pathname + window.location.search;
        const to_push = this.at_details_url(this.class_key);
        // If we're already at this address (such as by direct link)
        // then don't push it onto the history stack again
        if (current_loc !== to_push) {
          // Always replace state, so clicking through ATs doesn't
          // create new history entries
          window.history.replaceState(this.class_key, null, to_push);
        }
      },
      fetchData({pushstate=true} = {}) {
        if (!this.all_ats.length) {
          utils.fetchData(this.at_index_url(), (data) => {
            if (data) {
              this.pc_ats = data.player_archetypes;
              this.npc_ats = data.npc_archetypes;
            }
          });
        }
        if (!this.display_names) {
          utils.fetchData(this.base_data_url("display_names"), (data) => {
            if (data) {
              this.display_names = data;
            }
          });
        }

        if (this.class_key) {
          this.class_key = this.class_key.toLowerCase();
          if (pushstate) {
            this.setHistory();
          }
          utils.fetchData(this.at_data_url(this.class_key), (data) => {
            if (data) {
              this.at_data = data;
              setTitle(data);
            }
          });
        }
      },
      focusInput() {
        this.$nextTick(function() {
          this.$refs.picklist.focus();
          window.scrollTo(0,0);
        });
      },
      loadLocalStorage() {
        const saved_level = localStorage.getObject(LEVEL_STORAGE_NAME);
        if (saved_level) {
          this.display_level = saved_level;
        }
      },
      // Level input box validation
      valid_level($event) {
        if ($event.target.value < 1) {
          $event.preventDefault();
          $event.target.value = 1;
        }
        else if ($event.target.value > 54) {
          $event.preventDefault();
          $event.target.value = 54;
        }
        else return true;
      },
      display_at_name(name) {
        return utils.capitalize_words(name.replace(/_/g, " "));
      },
      display_attrib_name(name) {
        let display_name;
        if (this.display_names) {
          display_name = this.display_names.attribs[name];
        }
        if (!display_name) {
          return utils.capitalize_words(name.replace(/_|:/g, " "));
        }
        else {
          return display_name;
        }
      },
      display_mod_name(name) {
        let display_name;
        if (this.display_names) {
          display_name = this.display_names.attribmods[name];
        }
        if (!display_name) {
          return utils.capitalize_words(name.replace(/_|:/g, " "));
        }
        else {
          return display_name;
        }
      },
      // Lookup an attrib by name within its category
      // Some categories are 1-element lists, so we have to dereference
      // those. Everything returned here should be a number, but all are
      // cast as string or `null`, so that zero values aren't mistaken as
      // falsy, and any `undefined` that slip through are clearly unintended
      attrib_value(attrib_category, attrib_name) {
        if (!this.at_data) return null;

        let value;
        const [category, part] = attrib_category.split(":");
        let data = this.at_data[category];
        if (data) {
          if (part === "a") {
            data = data[0][0];
          }
          else if (part == "b"){
            data = data[1][0];
          }
          else {
            // Flatten other nested arrays
            while (Array.isArray(data)) {
              data = data[0];
            }
          }
          // Treat 'a:b' as a nested object::field
          const [type, subtype] = attrib_name.split(":");
          if (subtype) {
            const base = data[type];
            value = (base !== undefined) ? base[subtype] : null;
          }
          else {
            value = data[type];
          }
          if (value !== undefined && value !== null) {
            if (Array.isArray(value)) {
              value = value[this.level_offset];
              if (value !== undefined) {
                return value.fixedWidth(7, 3).toString();
              }
              else return null;
            }
            return value.toString();
          }
          return null;
        }
      },
      mod_value(mod_name) {
        const table = this.mod_tables.get(mod_name);
        if (table === undefined) {
          return null;
        }
        const value = table.values[this.level_offset];
        if (value === undefined) {
          return null;
        }
        return value.fixedWidth(7, 3).toString();
      },
    },
    computed: {
      level_offset() {
        return this.display_level - 1;
      },
      max_display_level() {
        return this.pc_ats.includes(this.class_key) ? 50 : 54;
      },
      all_ats() {
        const all_ats = [...this.pc_ats];
        all_ats.push(...this.npc_ats);
        return all_ats;
      },
      mod_tables() {
        const data = this.at_data;
        return new Map(Object.entries(data.named_tables));
      },
      mod_table_names() {
        return Array.from(this.mod_tables.keys())
          .sort()
          .map((x) => {
            return {
              display: utils.capitalize_words(x.replace(/_|:/g, " ")),
              name: x,
            };
          });
      },
      attribs() {
        if (!this.at_data) return [];

        const base = this.at_data.attrib_base[0];
        const attribs = [];
        for (const name in base) {
          if (name.endsWith("_type") || name === "elusivity") {
            for (const subtype in base[name]) {
              attribs.push(`${name}:${subtype}`);
            }
          }
          else {
            attribs.push(name);
          }
        }
        return attribs.sort();
      },
      attrib_cats() {
        if (!this.at_data) return [];

        const categories = [];
        return Array.from(Object.keys(this.at_data)).filter(
          (x) => x.startsWith("attrib")
        );
      },
      cat_entries() {
        return [
          {
            name: "attrib_base",
            display: "Base"
          },
          {
            name: "attrib_min",
            display: "Min"
          },
          {
            name: "attrib_max",
            display: "Max"
          },
          {
            name: "attrib_max_max",
            display: "MaxMax"
          },
          {
            name: "attrib_strength_min",
            display: "StrMin"
          },
          {
            name: "attrib_strength_max",
            display: "StrMax"
          },
          {
            name: "attrib_resistance_min",
            display: "ResMin"
          },
          {
            name: "attrib_resistance_max",
            display: "ResMax"
          },
          {
            name: "attrib_diminishing_cur:a",
            display: "DimCurA"
          },
          {
            name: "attrib_diminishing_cur:b",
            display: "DimCurB"
          },
          {
            name: "attrib_diminishing_str:a",
            display: "DimStrA"
          },
          {
            name: "attrib_diminishing_str:b",
            display: "DimStrB"
          },
          {
            name: "attrib_diminishing_res:a",
            display: "DimResA"
          },
          {
            name: "attrib_diminishing_res:b",
            display: "DimResB"
          },
        ];
      },
    },
    watch: {
      class_key: function(data) {
        this.fetchData({pushstate:true});
      },
      all_ats: function(data) {
          this.focusInput();
      },
      display_level: function(value) {
        localStorage.setObject(LEVEL_STORAGE_NAME, value);
      },
      max_display_level: function(value) {
        if (this.display_level > value) {
          this.display_level = value;
        }
      },
    },
    created() {
      // Attach onpopstate event handler
      window.onpopstate = (event)=> {
        const params = (new URL(window.location)).searchParams;
        const class_key = params.get("at");
        if (class_key === null) {
          this.class_key = "";
          this.at_data = null;
          document.title = "City of Data";
        }
        else {
          this.class_key = class_key;
          this.fetchData({pushstate:false});
        }
      };
    },
    mounted() {
      this.loadLocalStorage();
      const params = (new URL(window.location)).searchParams;
      const class_key = params.get("at");
      console.log(class_key);
      if (class_key) {
        this.class_key = class_key;
      }
      this.fetchData();
      if (this.event_bus) {
        // Set up handler for checkbox update events
        this.event_bus.$on("update-checkbox", ([checkbox, value]) => {
          Vue.set(this, checkbox, value);
        });
        // Send event requesting the current checkbox settings (as events)
        setTimeout(
          () => {
            this.event_bus.$emit("request-checkbox-settings");
          }, 5
        );
      }
    },
  };
};
