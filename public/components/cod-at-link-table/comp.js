/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

const AT_ROWS = [
  ["Blaster", "Controller", "Defender", "Scrapper", "Tanker"],
  ["Brute", "Corruptor", "Dominator", "Mastermind", "Stalker"],
  ["Peacebringer", "Warshade", "Sentinel", "Soldier", "Widow"],
];

function is_arachnos(name) {

  return ["soldier", "widow"].includes(name);
}

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-at-link-table");
  return {
    name: "cod-at-link-table",
    template: template,
    mixins: [urls_mixin],
    data() {
      return {};
    },
    props: {
    },
    methods: {
      * at_objects(row) {
        for (const at_name of row) {
          let lc_name = at_name.toLowerCase();
          if (is_arachnos(lc_name)) lc_name = `arachnos_${lc_name}`;
          yield {
            name: at_name,
            url: this.at_url(lc_name),
            icon: this.asset_url(`ats/${lc_name}.png`),
          };
        }
      },
    },
    computed: {
      rows() {
        return AT_ROWS;
      },
    },
  };
};
