/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import checkbox_mixin from "/components/cod-checkbox-base/libs/checkbox_mixin.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-checkbox-raw-data");
  return {
    name: "cod-checkbox-raw-data",
    template: template,
    mixins: [checkbox_mixin, urls_mixin],
    props: {
      name: {
        type: String,
        default: "show_raw_data",
      },
    },
  };
};
