/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

import SearchPicker from "/components/search-picker/comp.js";

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-entity-picker");
  return {
    name: "cod-entity-picker",
    template: template,
    mixins: [urls_mixin],
    components: {
      "search-picker": SearchPicker,
    },
    data() {
      return {
        all_entities: null,
        dropdown_data: undefined,
        initial_value: null,
        search_param: null,
        entity_fullname: null,
        ready: false,
        picker_ready: false,
      };
    },
    props: {
      max_items: {
        type: Number,
        default: 20,
      },
    },
    computed: {
    },
    methods: {
      setSelected(picker_obj) {
        console.log("Selecting", picker_obj.value);
        this.entity_fullname = picker_obj.value;
        this.setHistory();
        this.$emit("selected-entity-changed", picker_obj);
      },
      setHistory() {
        if (this.boostset_fullname !== null) {
          this.entity_fullname = this.entity_fullname.toLowerCase();
          const current_loc = window.location.pathname + window.location.search;
          const to_push = this.entity_url(this.entity_fullname);
          window.location = to_push;
        }
      },
      processEntities(data) {
        const options = [];
        for (const entry of this.all_entities) {
          options.push({
            value: entry[1],
            option: entry[0],
          });
        }
        this.all_entities = null;
        this.dropdown_data =  options.sort((a, b) => {
          if (a.option > b.option) return 1;
          if (a.option < b.option) return -1;
          return 0;
        });
      },
      itemFromValue(value) {
        for (const item of this.dropdown_data) {
          if (item.value === value) {
            return item;
          }
        }
        return null;
      },
      fetchData() {
        utils.fetchData(this.base_data_url("entity_names"), (data) => {
          this.all_entities = data;
          this.processEntities();
          if (this.search_param) {
            this.initial_value = this.itemFromValue(this.search_param);
          }
          this.ready = true;
        });
      },
    },
    created() {
      // Attach onpopstate event handler
      window.onpopstate = (event)=> {
        const params = (new URL(window.location)).searchParams;
        const full_name = params.get("entity");
        if (full_name === null) {
          this.search_param = "";
          document.title = "City of Data";
        }
        else {
          this.search_param = full_name;
          this.fetchData();
        }
      };
    },
    mounted() {
      const params = (new URL(window.location)).searchParams;
      const full_name = params.get("entity");
      if (full_name) {
        this.search_param = full_name;
      }
      this.fetchData();
    },
    watch: {
    },
  };
};
