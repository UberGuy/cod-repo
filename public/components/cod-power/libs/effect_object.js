/*jshint esversion: 6 */

import "/js/number_shim.js";
import { checkArray } from "/js/utils.js";


// Enum stand-in
export const PVE_OR_PVP = {

  PVE_ONLY: "PVE_ONLY",
  PVP_ONLY: "PVP_ONLY",
  EITHER: "EITHER",
};

export const EffectObject = {

  pvp_or_pve() {

    return this.is_pvp;
  },

  visbility_based_on_pvp_pve(display_setting) {
    let effect_setting = this.pvp_or_pve();
    return (
      effect_setting === PVE_OR_PVP.EITHER ||
      display_setting === PVE_OR_PVP.EITHER ||
      effect_setting === display_setting
    );
  },

  is_pvp_only() {

    return this.pvp_or_pve() === PVE_OR_PVP.PVP_ONLY;
  },

  is_pve_only() {

    return this.pvp_or_pve() === PVE_OR_PVP.PVE_ONLY;
  },

  is_either_pve_or_pvp() {

    return this.pvp_or_pve() === PVE_OR_PVP.EITHER;
  },

  effect_chance() {

    if (this.ppm !== 0) {
      return `${this.ppm} PPM`;
    }

    if (this.chance >= 1.0) {
      return `${(this.chance * 100).toPlaces(0)}% chance`;
    }

    return `${(this.chance * 100).toPlaces(1)}% chance`;
  },

  has_children() {
    return checkArray(this.child_effects);
  },

  // Returns mods sorted alphabetically by their attribs field
  sorted_mods() {
    return [...this.templates].sort((a, b) => a.attribs > b.attribs ? 1 : -1);
  },
};


function recursive_decorate(effects) {

  let new_effects = [];
  for (const effect of effects) {
    if (checkArray(effect.child_effects)) {
      effect.child_effects = recursive_decorate(effect.child_effects);
    }
    new_effects.push(Object.assign(effect, EffectObject));
  }
  return new_effects;
}

// Turn all the effect group objects in a power object into actual
// EffectObject instances, so they pick up useful methods. Returns
// the modified power object, but it's modified in-place.
export function decorate_effects(power_data) {

  power_data.effects = recursive_decorate(power_data.effects);
  power_data.activation_effects = recursive_decorate(power_data.activation_effects);
  return power_data;
}
