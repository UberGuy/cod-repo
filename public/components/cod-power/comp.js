/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import { PVE_OR_PVP } from "/components/cod-power/libs/effect_object.js";
import { EffectObject, decorate_effects } from "/components/cod-power/libs/effect_object.js";

import urls_mixin from "/components/cod-links/libs/urls_mixin.js";
import tippy_mixin from "/components/cod-tippy/libs/tippy_mixin.js";

import AtPicker from "/components/cod-at-picker/comp.js";
import BoostsAllowed from "/components/cod-boosts/comp.js";
import EffectTree from "/components/cod-effect/comp.js";
import PowerIcons from "/components/cod-power-icons/comp.js";

const VueSlider = window['vue-slider-component'];

// Change weird plurals
const BOOSTSET_NAME_REMAP = {
  "Holds": "Hold",
  "Stuns": "Stun",
  "Sniper Attacks": "Sniper Attack",
};

const LEVEL_STORAGE_NAME = "power-display-level";
const FREEFORM_STORAGE_NAME = "power-freeform-class";

function setTitle(power_data) {

  document.title = `Power '${power_data.display_name}' (${power_data.full_name})`;
}

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-power");
  return {
    name: "cod-power",
    template: template,
    mixins: [urls_mixin, tippy_mixin],
    components: {
      "effect-tree": EffectTree,
      "power-icons": PowerIcons,
      "boosts-allowed": BoostsAllowed,
      "at-picker": AtPicker,
      "vue-slider": VueSlider,
    },
    data() {
      return {
        at_tables: undefined,
        power_path: "",
        power_data: null,
        input_key: false,
        show_raw_data: false,
        show_debug_data: false,
        show_pvp_or_pve: false,
        show_pvp_and_pve: false,
        query_at: null,
        pc_ats: [],
        display_level: 50,
        freeform_classes: false,
      };
    },
    props: {
      event_bus: {
        type: Vue,
        required: false,
      },
    },
    methods: {
      setHistory() {
        const params = (new URL(window.location)).searchParams;
        const at_name = params.get("at");
        const power_name = params.get("power");
        let to_push = this.power_url(this.power_path, this.query_at);
        // If we're already at this address (such as by direct link)
        // then don't push it onto the history stack again
        if (this.power_path !== power_name || this.query_at !== at_name) {
          // If we're redirecting to a lower-cased version of the
          // same path, just overwrite the location. Don't consider an
          // AT change to be a new path.
          const lc_at_name = (at_name && at_name.toLowerCase()) || null;
          if (power_name.toLowerCase() === this.power_path) {
            window.history.replaceState(this.power_path, null, to_push);
          }
          else {
            window.history.pushState(this.power_path, null, to_push);
          }
        }
      },
      fetchData({pushstate=true} = {}) {
        if (!this.pc_ats.length) {
          utils.fetchData(this.at_index_url(), (data) => {
            if (data) {
              this.pc_ats = data.player_archetypes;
            }
          });
        }
        if (this.power_path !== null) {
          this.power_path = this.power_path.toLowerCase();
          let pcat, pset, power;
          [pcat, pset, power] = this.power_path.split(".");
          if (pushstate) {
            this.setHistory();
          }
          utils.fetchData(this.power_data_url(pcat, pset, power), (response) => {
            this.power_data = decorate_effects(response);
            if (this.power_data !== undefined) {
              setTitle(this.power_data);
            }
          });
        }
      },
      loadLocalStorage() {
        const saved_level = localStorage.getObject(LEVEL_STORAGE_NAME);
        if (saved_level) {
          this.display_level = saved_level;
        }
        const freeform_classes = localStorage.getObject(FREEFORM_STORAGE_NAME);
        if (saved_level) {
          this.freeform_classes = freeform_classes;
        }
      },
      switch_at(at_tables) {
        this.at_tables = at_tables;
        this.query_at = at_tables ? at_tables.$archetype: "none";
        this.setHistory();
      },
      // Level input box validation
      valid_level($event) {
        if ($event.target.value < 1) {
          $event.preventDefault();
          $event.target.value = 1;
        }
        else if ($event.target.value > this.max_display_level) {
          $event.preventDefault();
          $event.target.value = this.max_display_level;
        }
        else return true;
      },
    },
    computed: {
      icon_path() {
        return this.base_icon_url(this.power_data.icon);
      },
      pvp_or_pve() {
        return this.show_pvp_and_pve ? PVE_OR_PVP.EITHER : (
          this.show_pvp_or_pve ? PVE_OR_PVP.PVP_ONLY : PVE_OR_PVP.PVE_ONLY
        );
      },
      arcanatime() {
        return (Math.ceil(this.power_data.activation_time/0.132)+1) * 0.132;
      },
      damage() {
        // TODO: Calculate max/min/average damage
        return 0;
      },
      dpa() {
        return (this.damage / this.arcanatime);
      },
      desription_sans_markup() {
        const tmp = document.implementation.createHTMLDocument().body;
        tmp.innerHTML = this.power_data.display_help.replace(/<br>/g, "\n");
        return (tmp.textContent || tmp.innerText || "").replace(/^\*/gm, "● ");
      },
      power_links() {
        let pcat, pset, power, pcat_name, pset_name, power_name;
        [pcat, pset, power] = this.power_path.split(".");
        [pcat_name, pset_name, power_name] = this.power_data.display_fullname.split(".");
        const pcat_link = `<a href="${this.powercat_url(pcat)}">${pcat_name}</a>`;
        const pset_link = `<a href="${this.powerset_url(pcat + "." + pset, this.query_at)}">${pset_name}</a>`;
        return `<div class=title-tip>${pcat_link}.${pset_link}.${power_name}</div>`;
      },
      max_display_level() {
        return this.pc_ats.includes(this.query_at) ? 50 : 54;
      },
    },
    watch: {
      at_tables: function(tables) {
        if (tables === undefined) console.log("Switched raw scales.");
        else console.log("Switched to tables for:", this.at_tables.$archetype);
      },
      display_level: function(value) {
        localStorage.setObject(LEVEL_STORAGE_NAME, value);
      },
      freeform_classes: function(value) {
        localStorage.setObject(FREEFORM_STORAGE_NAME, value);
      },
      max_display_level: function(value) {
        if (this.display_level > value) {
          this.display_level = value;
        }
      },
    },
    created() {
      // Attach onpopstate event handler
      window.onpopstate = (event)=> {
        const params = (new URL(window.location)).searchParams;
        console.log(params);

        const at_name = params.get("at");
        if (at_name) {
          this.query_at = at_name.toLowerCase();
        }

        const full_name = params.get("power");
        if (full_name == null) {
          this.power_path = "";
          this.power_data = null;
          this.input_key = !this.input_key;
          document.title = "City of Data";
        }
        else {
          this.power_path = full_name;
          this.input_key = !this.input_key;
          this.fetchData({pushstate:false});
        }
      };
    },
    mounted() {
      this.loadLocalStorage();
      const params = (new URL(window.location)).searchParams;
      const at_name = params.get("at");
      if (at_name) {
        this.query_at = at_name.toLowerCase();
      }
      const full_name = params.get("power");
      if (full_name) {
        this.power_path = full_name;
        this.input_key = !this.input_key;
        this.fetchData();
        if (this.event_bus) {
          // Set up handler for checkbox update events
          this.event_bus.$on("update-checkbox", ([checkbox, value]) => {
            Vue.set(this, checkbox, value);
          });
          // Send event requesting the current checkbox settings (as events)
          setTimeout(
            () => {
              this.event_bus.$emit("request-checkbox-settings");
            }, 5
          );
        }
      }
    }
  };
};
