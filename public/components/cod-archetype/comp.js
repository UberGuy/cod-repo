/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

function setTitle(at_data) {

  document.title = `Archetype '${at_data.display_name}' (${at_data.name})`;
}

// Compare two entry objects
function compare(a, b) {

  if (a.display > b.display) return 1;
  if (a.display < b.display) return -1;
  if (a.fullname > b.fullname) return 1;
  if (a.fullname < b.fullname) return -1;
  return 0;
}

function get_powersets(catagory_obj) {

  let powersets = [];
  for (const [idx, fullname] of catagory_obj.powerset_names.entries()) {
    const entry = {
      display: catagory_obj.powerset_display_names[idx],
      fullname: fullname.toLowerCase(),
    };
    powersets.push(entry);
  }
  return powersets;
}

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-archetype");
  return {
    name: "cod-archetype",
    template: template,
    mixins: [urls_mixin],
    components: {
    },
    data() {
      return {
        at_data: null,
        powers_data: null,
        pri_pcat: null,
        sec_pcat: null,
        pool_pcat: null,
        class_key: "",
        show_raw_data: false,
      };
    },
    props: {
      event_bus: {
        type: Vue,
        required: true,
      },
    },
    methods: {
      setHistory() {
        const current_loc = window.location.pathname + window.location.search;
        const to_push = this.at_url(this.class_key);
        // If we're already at this address (such as by direct link)
        // then don't push it onto the history stack again
        if (current_loc !== to_push) {
          // If we're redirecting to a lower-cased version of the
          // same path, just overwrite the location
          if (current_loc.toLowerCase() === to_push) {
            window.history.replaceState(this.class_key, null, to_push);
          }
          else {
            window.history.pushState(this.class_key, null, to_push);
          }
        }
      },
      fetchData({pushstate=true} = {}) {
        if (this.class_key !== null) {
          this.class_key = this.class_key.toLowerCase();
          if (pushstate) {
            this.setHistory();
          }
          utils.fetchData(this.at_data_url(this.class_key), (data) => {
            if (data) {
              this.at_data = data;
              setTitle(data);
            }
          });
          utils.fetchData(this.base_data_url("powers_by_at"), (data) => {
            if (data) {
              this.powers_data = data;
            }
          });        }
      },
      powerset_link(index) {
        const display_name = this.at_data.powerset_display_names[index].replace(/_/g, " ");
        const full_name = this.at_data.powerset_names[index];
        const internal_name = full_name.split(".")[1].replace(/_/g, " ");
        if (full_name) {
          return `<a href="${this.powerset_url(full_name)}">${display_name || internal_name}</a>`;
        }
        return "—";
      },
    },
    computed: {
      ready() {
        return !!(
          this.at_data &&
          this.powers_data &&
          this.pri_pcat &&
          this.sec_pcat &&
          this.pool_pcat
        );
      },
      primary_powersets() {
        if (!this.pri_pcat) return [];

        return get_powersets(this.pri_pcat).sort(compare);
      },
      secondary_powersets() {
        if (!this.sec_pcat) return [];

        return get_powersets(this.sec_pcat).sort(compare);
      },
      pool_powersets() {
        if (!this.pool_pcat) return [];

        return get_powersets(this.pool_pcat).sort(compare);
      },
      epic_powersets() {
        if (!this.powers_data) return [];

        let powersets = new Map();
        for (const [display, fullname] of this.powers_data[this.class_key]) {
          if (fullname.startsWith("epic")) {
            const pset_name = fullname.split(".").slice(0,2).join(".");
            if (!powersets.has(pset_name)) {
              const entry = {
                display: display.split(".")[1],
                fullname: pset_name,
              };
              powersets.set(pset_name, entry);
            }
          }
        }
        return [...powersets.values()].sort(compare);
      },
      inherent_powers() {
        if (!this.powers_data) return [];

        let powers = new Map();
        for (const [display, fullname] of this.powers_data[this.class_key]) {
          if (fullname.startsWith("inherent")) {
            const entry = {
              display: display.split(".")[2],
              fullname,
            };
            powers.set(fullname, entry);
          }
        }
        return [...powers.values()].sort(compare);
      },
      power_dupes() {
        let dupes = new Map();
        for (const power_obj of this.inherent_powers) {
          const key = power_obj.display;
          dupes.set(key, (dupes.get(key) || 0) + 1);
        }
        return dupes;
      },
    },
    watch: {
      // Load powercat data once the AT data is loaded
      // We have to wait for the AT data to know the powercat names
      at_data: function(data) {
        const pri_pcat = data.primary_category.toLowerCase();
        const sec_pcat = data.secondary_category.toLowerCase();
        const pool_pcat = data.power_pool_category.toLowerCase();

        utils.fetchData(this.pcat_data_url(pri_pcat), (data) => {
          if (data) this.pri_pcat = data;
        });
        utils.fetchData(this.pcat_data_url(sec_pcat), (data) => {
          if (data) this.sec_pcat = data;
        });
        utils.fetchData(this.pcat_data_url(pool_pcat), (data) => {
          if (data) this.pool_pcat = data;
        });
      },
    },
    created() {
      // Attach onpopstate event handler
      window.onpopstate = (event)=> {
        const params = (new URL(window.location)).searchParams;
        const class_key = params.get("at");
        if (class_key === null) {
          this.class_key = "";
          this.at_data = null;
          document.title = "City of Data";
        }
        else {
          this.class_key = class_key;
          this.fetchData({pushstate:false});
        }
      };
    },
    mounted() {
      const params = (new URL(window.location)).searchParams;
      const class_key = params.get("at");
      console.log(class_key);
      if (class_key) {
        this.class_key = class_key;
        this.fetchData();
      }
      if (this.event_bus) {
        // Set up handler for checkbox update events
        this.event_bus.$on("update-checkbox", ([checkbox, value]) => {
          Vue.set(this, checkbox, value);
        });
        // Send event requesting the current checkbox settings (as events)
        setTimeout(
          () => {
            this.event_bus.$emit("request-checkbox-settings");
          }, 5
        );
      }
    }
  };
};
