/*jshint esversion: 8 */

import {fetch_template} from "/js/utils.js";

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await fetch_template("cod-footer");
  return {
    name: "cod-footer",
    template: template,
    data() {
      return {
      };
    },
  };
};
