/*jshint esversion: 8 */

import {fetch_template} from "/js/utils.js";
import icon_mixin from "/components/cod-mod-icons/libs/icons_mixin.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";
import tippy_mixin from "/components/cod-tippy/libs/tippy_mixin.js";

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await fetch_template("cod-mod-icons");
  return {
    name: "mod-icons",
    template: template,
    mixins: [icon_mixin, urls_mixin, tippy_mixin],
    props: {
      attribmod_data: {
        type: Object,
        required: true,
      },
    },
    data() {
      return {
      };
    },
    methods: {
    },
    computed: {
      flags() {
        return this.attribmod_data.flags.map(x => x.split(" ")[0]);
      }
    },
  };
};
