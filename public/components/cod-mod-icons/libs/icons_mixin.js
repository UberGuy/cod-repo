/*jshint esversion: 8 */

export default {

  computed: {

    caster_stacking() {
      const stack = this.attribmod_data.caster_stack;
      if (stack === 'Individual' || this.flags.includes("StackExactPower")) {
        return {
          icon: "caster_stack.png",
          content: "Stacking is per-caster",
        };
      }
      if (stack === 'Collective') {
        return {
          icon: "collective_stack.png",
          content: "Stacking is collective",
        };
      }
      // TODO: Return something here that indicates we fell through
      return false;
    },

    stacking_type() {
      const stack = this.attribmod_data.stack;
      if (["Stack", "Continuous"].includes(stack)) {
        return {
          icon: "stack.png",
          content: "Stacks with existing effect",
        };
      }
      if (stack === "Replace") {
        return {
          icon: "replace.png",
          content: "Replaces existing effect",
        };
      }
      if (stack === "StackToLimit") {
        return {
          icon: "capped-stack.png",
          content: "Stacks with existing effect<br>" +
                   `to a maximum of ${this.attribmod_data.stack_limit} stacks`,
        };
      }
      if (stack === "Ignore") {
        return {
          icon: "stack-ignore.png",
          content: "No stacking and new effect ignored",
        };
      }
      if (stack === "Extend") {
        return {
          icon: "stack-extend.png",
          content: "Stacks extend current effect",
        };
      }
      if (stack === "Overlap") {
        return {
          icon: "stack-overlap.png",
          content: "Stacks increase effect but don't extend duration",
        };
      }
      if (stack === "Refresh") {
        return {
          icon: "stack-extend.png",
          content: "Stacks and updates durations of current effects",
        };
      }
      if (stack === "Suppress") {
        return {
          icon: "stack-suppress.png",
          content: "Highest value suppresses others",
        };

      }
      if (stack === "RefreshToCount") {
        return {
          icon: "stack-extend.png",
          content: "Stacks and updates durations of current effects<br>" +
          `to a maximum of ${this.attribmod_data.stack_limit} stacks`,
        };
      }
      // We don't know what this value meant
      return {
        icon: "question-mark.png",
        content: "Unknown stacking type",
      };
    },

    stacking_key() {
      if (this.attribmod_data.stack_key) {
        let tagline;
        if (this.flags.includes("StackByAttribAndKey")) {
          tagline = "Stacks per<br>attribute by key";
        }
        else {
          tagline = "Stacks by key";
        }
        return {
          icon: "stack-key.png",
          content: '<div class="tags"><div class="tags-tip">\n' + 
                   `${tagline}\n` +
                   `<span><b>${this.attribmod_data.stack_key}</b></span>\n`+
                   "</div></div>\n",
        };
      }
      return false;
    },

    stacks_by_power() {
      if (this.flags.includes("StackExactPower")) {
        return {
          icon: "stacks_by_power.png",
          content: "Stacks by power",
        };
      }
      return false;
    },

    recharge_power() {
      if (this.attribmod_data.attribs.includes('Recharge_Power')) {
        if (this.flags.includes('Cooldown')) {
          return {
            icon: "recharge-cooldown.png",
            content: "Affected powers start recharging as if cast",
          };
        }
        if (this.flags.includes('SetTimer')) {
          return {
            icon: "recharge-timer.png",
            content: "Affected power recharges in time displayed",
          };
        }
        // TODO: Confirm this is the only remaining case
        return {
          icon: "recharge-now.png",
          content: "Affected powers immediately recharge",
        };
      }
      return false;
    },

    unresistable() {
      if (this.flags.includes("IgnoreResistance")) {
        return {
          icon: "unresistable.png",
          content: "Ignores resistance",
        };
      }
      return false;
    },

    ignores_strength() {
      if (this.flags.includes("IgnoreStrength")) {
        return {
          icon: "ignores_strength.png",
          content: "Ignores enhancements and other boosts",
        };
      }
      return false;
    },

    ignores_combatmods() {
      if (this.flags.includes("IgnoreLevelDifference")) {
        return {
          icon: "no-combat-mods.png",
          content: "Does not scale with enemy level",
        };
      }
      return false;
    },

    cancel_on_miss() {
      if (this.flags.includes("CancelOnMiss")) {
        return {
          icon: "cancel-on-miss.png",
          content: "A tick that misses its chance<br>cancels remaining ticks"
        };
      }
      return false;
    },

    keep_through_death() {
      if (this.flags.includes("KeepThroughDeath")) {
        return {
          icon: "keep-through-death.png",
          content: "Effect persists through death"
        };
      }
      return false;
    },

    tags() {
      const params = this.attribmod_data.params;
      if (params) {
        if (params.type === "EffectFilter") {
          return {
            icon: "tags.png",
            content: '<div class="tags"><div class="tags-tip">\n' +
                     '<strong>Affects Tags</strong>\n' +
                      Array.from(params.tags, (tag) =>
                        `<a href="${this.tag_url(tag, "bears")}">${tag}</a>`
                      ).join("<br>\n") +
                       "</div></div>\n"
          };
        }
      }
      return false;
    },

    creates_pseudopet() {
      if (this.flags.includes("PseudoPet")) {
        return {
          icon: "pet.png",
          content: "Summons a pseudopet"
        };
      }
      return false;
    },

    copies_mods() {
      if (this.flags.includes("CopyCreatorMods") || this.flags.includes("PseudoPet")) {
        return {
          icon: "copy-red.png",
          content: "Entity has creator's modifiers"
        };
      }
      return false;
    },

    copies_boosts() {
      if (this.flags.includes("CopyBoosts")) {
        return {
          icon: "copy-blue.png",
          content: "Boosts are copied to entity"
        };
      }
      return false;
    },

    // Not in use in any power
    // commandable_pet() {
    //   if (this.flags.includes("PetCommandable")) {
    //     return {
    //       icon: "command.png",
    //       content: "Pet is commandable"
    //     };
    //   }
    //   return false;
    // },

    cancelled_by() {
      const cancellations = this.attribmod_data.cancel_events;
      if (cancellations.length) {
        return {
          icon: "cancel.png",
          content: '<div class="tags"><div class="tags-tip">\n' +
                   '<strong>Cancel When</strong>\n' +
                   '<span>\n' +
                    cancellations.join("<br>\n") +
                    '</span>\n' +
                    "</div></div>\n"
        };
      }
      return false;
    },

    suppressed_by() {
      const suppressions = this.attribmod_data.suppress_events;
      if (suppressions.length) {
        return {
          icon: "suppress.png",
          content: '<div class="tags"><div class="tags-tip">\n' +
                   '<strong>Suppressed When</strong>\n' +
                   '<span>\n' +
                   Array.from(suppressions, (event) =>
                     `${event.event} for ${event.post_event_delay}s` +
                     (event.always_suppress ? " (Always)" : "")
                   ).join("<br>\n") +
                   '</span>\n' +
                   "</div></div>\n"
        };
      }
      return false;
    },

    no_floaters() {
      if (this.flags.includes("NoFloaters")) {
        return {
          icon: "no-floaters.png",
          content: "No float text for effect",
        };
      }
      return false;
    },

  },
};
