/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

const LOOKUP_TYPES = new Set([
  "affects",
  "bears",
]);

function setTitle(tag_name) {

  if (tag_name) {
    document.title = `Tag '${tag_name}'`;
  }
  else {
    document.title = "Tags";
  }
}

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-power-tag");
  return {
    name: "cod-power-tag",
    template: template,
    mixins: [urls_mixin],
    components: {
    },
    data() {
      return {
        power_map: null,
        tag_name: null,
        list_type: null,
      };
    },
    props: {
    },
    methods: {
      fetchData({pushstate=true} = {}) {
        if (this.tag_name !== null) {
          this.tag_name = this.tag_name.toLowerCase();
          if (pushstate) {
            const params = (new URL(window.location)).searchParams;
            const tag_name = params.get("tag");
            const query = params.get("q");
            const to_push = this.tag_url(this.tag_name, this.list_type);
            // If we're already at this address (such as by direct link)
            // then don't push it onto the history stack again
            if (query === this.list_type) {
              // If we're redirecting to a lower-cased version of the
              // same path, just overwrite the location
              if (tag_name.toLowerCase() === this.tag_name) {
                window.history.replaceState(this.tag_name, null, to_push);
              }
              else {
                window.history.pushState(this.tag_name, null, to_push);
              }
            }
          }

          utils.fetchData(this.tag_data_url(this.tag_name), (data) => {
            this.power_map = data;
            if (data) {
              setTitle(this.power_map.tag);
            }
            this.ready = true;
          });
        }
      },
    },
    computed: {
      power_list() {
        return this.power_map[this.list_type] || [];
      }
    },
    watch: {
    },
    created() {
      // Attach onpopstate event handler
      window.onpopstate = (event)=> {
        const params = (new URL(window.location)).searchParams;
        const tag_name = params.get("tag");
        const query = params.get("q");
        if (tag_name === null || query === null) {
          document.title = "City of Data";
        }
        else {
          this.tag_name = tag_name;
          this.list_type = query;
          this.fetchData({pushstate:false});
        }
      };
    },
    mounted() {
      const params = (new URL(window.location)).searchParams;
      const tag_name = params.get("tag");
      const query = params.get("q");
      if (tag_name !== null & query !== null) {
        this.tag_name = tag_name;
        this.list_type = query;
        this.fetchData();
      }
    }
  };
};
