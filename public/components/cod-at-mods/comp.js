/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

const { DataTable, ItemsPerPageDropdown, Pagination } = window['v-datatable-light'];

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-at-mods");
  return {
    name: "cod-at-mods",
    template: template,
    mixins: [urls_mixin],
    components: {
      "data-table": DataTable,
    },
    data() {
      return {
        archetypeTables: new Map(),
        currentAttrib: null,
        ready: false,
      };
    },
    props: [
      "event_bus",
    ],
    methods: {
      fetchData() {
        const promises = [];
        for (const header of this.headers) {
          if (header.skipLoad) continue;
          const data_path = this.mod_table_url(header.name);
          console.log(`Fetching ${data_path}`);
          const promise = utils.fetchData(data_path, (data) => {
            let tables = new Map(Object.entries(data.named_tables));
            this.archetypeTables.set(header.name, tables);
          });
          promises.push(promise);
        }
        Promise.all(promises).then(() => {
          this.currentAttrib = this.table_names[0];
          this.ready = true;
        });
      },
      focusInput() {
        this.$nextTick(function() {
          this.$refs.picklist.focus();
          window.scrollTo(0,0);
        });
      }
    },
    computed: {
      headers() {
        return [
          {
            label: "Lvl",
            name: "level",
            skipLoad: true,
          },
          {
            label: "Blas",
            name: "blaster",
          },
          {
            label: "Cont",
            name: "controller",
          },
          {
            label: "Def",
            name: "defender",
          },
          {
            label: "Scra",
            name: "scrapper",
          },
          {
            label: "Tank",
            name: "tanker",
          },
          {
            label: "Khel",
            name: "peacebringer",
          },
          {
            label: "Sent",
            name: "sentinel",
          },
          {
            label: "Bru",
            name: "brute",
          },
          {
            label: "Corr",
            name: "corruptor",
          },
          {
            label: "Dom",
            name: "dominator",
          },
          {
            label: "MM",
            name: "mastermind",
          },
          {
            label: "Sta",
            name: "stalker",
          },
          {
            label: "SoA",
            name: "arachnos_widow",
          },
        ];
      },
      rows() {
        const rows = [];
        for (let level=0; level<55; level++) {
          let undef = true;
          const entry = {
            level: level + 1,
          };
          for (const header of this.headers) {
            if (header.skipLoad) continue;
            const table = this.archetypeTables.get(header.name);
            const value = table.get(this.currentAttrib)[level];
            if (value === undefined) {
              entry[header.name] = "–";
            }
            else {
              entry[header.name] = value;
              undef = false;
            }
          }
          if (!undef) {
            rows.push(entry);
          }
        }
        // Can't get the Vue table's sorting to work, so doing this here
        rows.reverse();
        return rows;
      },
      table_names() {
        // Grab the table names from a known-good AT
        const table = this.archetypeTables.get("blaster");
        return Array.from(table.keys()).sort();
      }
    },
    watch: {
      ready: function() {
        this.focusInput();
      },
    },
    created() {
      this.fetchData();
    },
    mounted() {
    },
  };
};
