/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

import SearchPicker from "/components/search-picker/comp.js";

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-boostset-picker");
  return {
    name: "cod-boostset-picker",
    template: template,
    mixins: [urls_mixin],
    components: {
      "search-picker": SearchPicker,
    },
    data() {
      return {
        all_boostsets: null,
        dropdown_data: undefined,
        initial_value: null,
        search_param: null,
        boostset_fullname: null,
        ready: false,
        picker_ready: false,
      };
    },
    props: {
      max_items: {
        type: Number,
        default: 20,
      },
    },
    computed: {
    },
    methods: {
      setSelected(picker_obj) {
        console.log("Selecting", picker_obj.value);
        this.boostset_fullname = picker_obj.value;
        this.setHistory();
        this.$emit("selected-boostset-changed", picker_obj);
      },
      setHistory() {
        if (this.boostset_fullname !== null) {
          this.boostset_fullname = this.boostset_fullname.toLowerCase();
          let current_loc = window.location.pathname + window.location.search;
          let to_push = this.boostset_url(this.boostset_fullname);
          window.location = to_push;
        }
      },
      processBoostsets(data) {
        let options = [];
        const sets = this.all_boostsets;
        for (const [index, entry] of sets.set_names.entries()) {
          options.push({
            value: entry,
            option: sets.set_display_names[index],
          });
        }
        this.all_boostsets = null;
        this.dropdown_data =  options.sort((a, b) => {
          if (a.option > b.option) return 1;
          if (a.option < b.option) return -1;
          return 0;
        });
      },
      itemFromValue(value) {
        for (const item of this.dropdown_data) {
          if (item.value === value) {
            return item;
          }
        }
        return null;
      },
      fetchData() {
        utils.fetchData(this.boostset_index_url(), (data) => {
          this.all_boostsets = data;
          this.processBoostsets();
          if (this.search_param) {
            this.initial_value = this.itemFromValue(this.search_param);
          }
          this.ready = true;
        });
      },
    },
    created() {
      // Attach onpopstate event handler
      window.onpopstate = (event)=> {
        const params = (new URL(window.location)).searchParams;
        const full_name = params.get("set");
        if (full_name === null) {
          this.search_param = "";
          document.title = "City of Data";
        }
        else {
          this.search_param = full_name;
          this.fetchData();
        }
      };
    },
    mounted() {
      const params = (new URL(window.location)).searchParams;
      const full_name = params.get("set");
      if (full_name) {
        this.search_param = full_name;
      }
      this.fetchData();
    },
    watch: {
    },
  };
};
