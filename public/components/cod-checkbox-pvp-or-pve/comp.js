/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import checkbox_mixin from "/components/cod-checkbox-base/libs/checkbox_mixin.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-checkbox-pvp-or-pve");
  return {
    name: "cod-checkbox-pvp-or-pve",
    template: template,
    mixins: [checkbox_mixin, urls_mixin],
    data() {
      return {
        checked: false, // show_pvp_or_pve value
        indeterminate: true, // show_pvp_and_pve state
      };
    },
    props: {
      name: {
        type: String,
        default: "show_pvp_or_pve",
      },
    },
    methods: {
      click_event() {
        if (this.indeterminate) {
          this.indeterminate = false;
          this.checked = true;
          return;
        }
        if (this.checked) {
          this.checked = false;
          this.indeterminate = false;
        }
        else {
          this.indeterminate = true;
        }
      },
      state(id) {
        return {
          checked: this.checked,
          indeterminate: this.indeterminate,
        };
      },
    },
    mounted() {
      // Load the checkboxes from localstorage
      this.checked = !!localStorage.getObject("show_pvp_or_pve");
      const indeterminate = localStorage.getObject("show_pvp_and_pve");
      // Show both PvE and PvP by default
      if (indeterminate === null) {
        this.indeterminate = true;
      }
      else {
        this.indeterminate = !!indeterminate;
      }

      // Register an event listener to handle requests to emit the current
      // status of all the checkboxes
      if (this.event_bus) {
        this.event_bus.$on("request-checkbox-settings", () => {
          this.on_update("show_pvp_or_pve", this.checked, true);
          this.on_update("show_pvp_and_pve", this.indeterminate, true);
        });
      }
    },
    watch: {
      checked: function(value) {
        this.on_update("show_pvp_or_pve", value);
      },
      indeterminate: function(value) {
        this.on_update("show_pvp_and_pve", value);
      },
    }
  };
};
