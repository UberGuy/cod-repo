/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";
import tippy_mixin from "/components/cod-tippy/libs/tippy_mixin.js";

function setTitle(powerset_data) {

  let pset_name;
  [ , pset_name , ] = powerset_data.display_fullname.split(".");
  document.title = `Powerset '${pset_name}' (${powerset_data.full_name})`;
}

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-powerset");
  return {
    name: "cod-powerset",
    template: template,
    mixins: [urls_mixin, tippy_mixin],
    components: {
    },
    data() {
      return {
        powerset_path: "",
        powerset_data: null,
        query_at: null,
        show_raw_data: false,
      };
    },
    props: {
      event_bus: {
        type: Vue,
        required: false,
      },
    },
    methods: {
      setHistory() {
        const params = (new URL(window.location)).searchParams;
        const at_name = params.get("at");
        const pset_name = params.get("pset");
        let to_push = this.power_url(this.powerset_path, this.query_at);
        // If we're already at this address (such as by direct link)
        // then don't push it onto the history stack again
        if (this.powerset_path !== pset_name || this.query_at !== at_name) {
          // If we're redirecting to a lower-cased version of the
          // same path, just overwrite the location. Don't consider an
          // AT change to be a new path.
          const lc_at_name = (at_name && at_name.toLowerCase()) || null;
          if (pset_name.toLowerCase() === this.powerset_path) {
            window.history.replaceState(this.powerset_path, null, to_push);
          }
          else {
            window.history.pushState(this.powerset_path, null, to_push);
          }
        }
      },
      fetchData({pushstate=true} = {}) {
        if (this.powerset_path !== null) {
          this.powerset_path = this.powerset_path.toLowerCase();
          let pcat, pset;
          [pcat, pset] = this.powerset_path.split(".");
          if (pushstate) {
            this.setHistory();
          }
          utils.fetchData(this.pset_data_url(pcat, pset), (response) => {
            this.powerset_data = response;
            if (response) {
              setTitle(response);
            }
          });
        }
      },
    },
    computed: {
      power_entries() {
        const entries = [];
        const data = this.powerset_data;
        for (const [index, full_name] of data.power_names.entries()) {
          const display_name = data.power_display_names[index];
          const available_level = data.available_level[index];
          if (display_name && full_name) {
            entries.push({
              fullName: full_name,
              display: display_name,
              level: available_level + 1,
            });
          }
        }
        return entries;
      },
      //
      powerset_links() {
        let pcat, pset, pcat_name, pset_name;
        [pcat, pset, ] = this.powerset_path.split(".");
        [pcat_name, pset_name, ] = this.powerset_data.display_fullname.split(".");
        const pcat_link = `<a href="${this.powercat_url(pcat)}">${pcat_name}</a>`;
        return `<div class=title-tip>${pcat_link}.${pset_name}</div>`;
      },
    },
    watch: {
    },
    created() {
      // Attach onpopstate event handler
      window.onpopstate = (event)=> {

        const at_name = params.get("at");
        if (at_name) {
          this.query_at = at_name.toLowerCase();
        }

        var params = (new URL(window.location)).searchParams;
        var full_name = params.get("pset");
        if (full_name === null) {
          this.powerset_path = "";
          this.powerset_data = null;
          document.title = "City of Data";
        }
        else {
          this.powerset_path = full_name;
          this.input_key = !this.input_key;
          this.fetchData({pushstate:false});
        }
      };
    },
    mounted() {
      var params = (new URL(window.location)).searchParams;
      const at_name = params.get("at");
      if (at_name) {
        this.query_at = at_name.toLowerCase();
      }
      var full_name = params.get("pset");
      if (full_name) {
        this.powerset_path = full_name;
        this.fetchData();
        if (this.event_bus) {
          // Set up handler for checkbox update events
          this.event_bus.$on("update-checkbox", ([checkbox, value]) => {
            Vue.set(this, checkbox, value);
          });
          // Send event requesting the current checkbox settings (as events)
          setTimeout(
            () => {
              this.event_bus.$emit("request-checkbox-settings");
            }, 5
          );
        }
      }
    }
  };
};
