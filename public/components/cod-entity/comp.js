/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

function setTitle(name, full_name) {

  document.title = `Entity '${name}' (${full_name})`;
}

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await utils.fetch_template("cod-entity");
  return {
    name: "cod-entity",
    template: template,
    mixins: [urls_mixin],
    components: {
    },
    data() {
      return {
        entity_data: null,
        entity_key: "",
        show_raw_data: false,
      };
    },
    props: {
      event_bus: {
        type: Vue,
        required: false,
      },
    },
    methods: {
      setHistory() {
        const current_loc = window.location.pathname + window.location.search;
        const to_push = this.entity_url(this.entity_key);
        // If we're already at this address (such as by direct link)
        // then don't push it onto the history stack again
        if (current_loc !== to_push) {
          // If we're redirecting to a lower-cased version of the
          // same path, just overwrite the location
          if (current_loc.toLowerCase() === to_push) {
            window.history.replaceState(this.powerset_path, null, to_push);
          }
          else {
            window.history.pushState(this.powerset_path, null, to_push);
          }
        }
      },
      fetchData({pushstate=true} = {}) {
        if (this.entity_key !== null) {
          this.entity_key = this.entity_key.toLowerCase();
          if (pushstate) {
            this.setHistory();
          }
          utils.fetchData(this.entity_data_url(this.entity_key), (data) => {
            if (data) {
              this.entity_data = data;
              setTitle(this.display_name, this.entity_data.name);
            }
          });
        }
      },
    },
    computed: {
      ready() {
        return !!(this.entity_data);
      },
      powers() {
        let powers = [];
        const data = this.entity_data;

        if (!data) return powers;

        for (const [idx, fullname] of data.power_full_names.entries()) {
          const entry = {
            display: data.power_display_names[idx],
            fullname: fullname.toLowerCase(),
            level: data.power_levels[idx]
          };
          powers.push(entry);
        }
        return powers;
      },
      power_dupes() {
        let dupes = new Map();
        for (const power_obj of this.powers) {
          const key = power_obj.display;
          dupes.set(key, (dupes.get(key) || 0) + 1);
        }
        return dupes;
      },
      display_name() {
        const levels = this.entity_data.levels;
        if (levels && levels.length) {
          const display_names = levels[levels.length -1].display_names;
          if (display_names && display_names.length) {
            return display_names[0];
          }
        }
        return this.entity_data.name;
      },
      levels() {
        return this.entity_data.levels.sort((a, b) => a.level - b.level);
      },
      name_ranges() {
        const name_ranges = new Map();
        const level_objs = this.levels;
        if (level_objs && level_objs.length) {
          for (const level_obj of level_objs) {
            const names = level_obj.display_names.join("/");
            let name_levels = name_ranges.get(names);
            if (!name_levels) {
              name_levels = [];
              name_ranges.set(names, name_levels);
            }
            name_levels.push(level_obj.level);
          }
          const name_levels = [];
          for (const [names, levels] of name_ranges) {
            name_levels.push([
              utils.int_array_to_ranges(levels)[0],
              names.split("/")
            ]);
          }
          return name_levels;
        }
        return name_ranges;
      },
      level_ranges() {
        const levels_objs = this.levels;
        if (levels_objs && levels_objs.length) {
          const levels = [];
          levels_objs.forEach(o => levels.push(o.level));
          return utils.int_array_to_ranges(levels);
        }
        return [];
      },
    },
    watch: {
    },
    created() {
      // Attach onpopstate event handler
      window.onpopstate = (event)=> {
        const params = (new URL(window.location)).searchParams;
        const entity_key = params.get("entity");
        if (entity_key === null) {
          this.entity_key = "";
          this.entity_data = null;
          document.title = "City of Data";
        }
        else {
          this.entity_key = entity_key;
          this.fetchData({pushstate:false});
        }
      };
    },
    mounted() {
      const params = (new URL(window.location)).searchParams;
      const entity_key = params.get("entity");
      console.log(entity_key);
      if (entity_key) {
        this.entity_key = entity_key;
        this.fetchData();
      }
      if (this.event_bus) {
        // Set up handler for checkbox update events
        this.event_bus.$on("update-checkbox", ([checkbox, value]) => {
          Vue.set(this, checkbox, value);
        });
        // Send event requesting the current checkbox settings (as events)
        setTimeout(
          () => {
            this.event_bus.$emit("request-checkbox-settings");
          }, 5
        );
      }
    }
  };
};
