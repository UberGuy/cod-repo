/*jshint esversion: 8 */

import * as utils from "/js/utils.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

// Return a Promise so we can retrieve the template externally
export default async () => {
  const template = await utils.fetch_template("cod-power-linker");
  return {
    name: "search-linker",
    template: template,
    mixins: [urls_mixin],
    data () {
      return {
      };
    },
    mounted () {
    },
    props: {
      fullname: {
        type: String,
        default: "",
      },
      displayname: {
        type: String,
        required: "",
      },
    },
    methods: {
    },
    computed: {
      powercat() {
        if (!this.displayname || !this.fullname) return "";
        const pcat_name = this.fullname.split(".")[0];
        return {
          display: this.displayname.split(".")[0],
          urlTarget: this.powercat_url(pcat_name),
        };
      },
      powerset() {
        if (!this.displayname || !this.fullname) return "";
        const pset_name = this.fullname.split(".").slice(0,2).join(".");
        return {
          display: this.displayname.split(".").slice(0,2).join("."),
          urlTarget: this.powerset_url(pset_name),
        };
      },
      power() {
        if (!this.displayname || !this.fullname) return "";
        return {
          display: this.displayname,
          urlTarget: this.power_url(this.fullname),
        };
      },
    },
    watch: {
    },
  };
};
