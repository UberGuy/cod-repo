/*jshint esversion: 6 */

import "/js/number_shim.js";
import {checkArray, setsEqual, snake_to_ucamel_case} from "/js/utils.js";

// TODO: Make this a Map
const TARGET_STRINGS = {
  "Self": "self only",
  "SelfAndPets": "self and own pets",
  "AnyAffected": "all affected targets",
  "AnyAffectedAndPets": "all affected targets and their pets",
  "MainTargetOnly": "the target of the power",
  "TargetedAndPets": "the target of the power and their pets",
};

export const ALL_STD_DAMAGE = new Set([
  "Smashing Dmg", "Lethal Dmg", "Fire Dmg", "Cold Dmg",
  "Energy Dmg", "Negative Energy Dmg", "Psionic Dmg", "Toxic Dmg"
]);

export const ALL_DAMAGE = new Set([
  "Special Dmg", "Unique1 Dmg", "Unique2 Dmg", "Unique3 Dmg", "Quantum Dmg",
   ...ALL_STD_DAMAGE,
]);

export const ALL_DEFENSE = new Set([
  "Smashing", "Lethal", "Fire", "Cold",
  "Energy", "Negative Energy", "Psionic", "Toxic",
  "Melee", "Ranged", "Area",
]);

export const DIRECT_EFFECT = new Set([
  "Execute Power", "Recharge Power", "Grant Power", "Reward",
  "Revoke Power", "Create Entity", "Set Mode", "Unset Mode",
  "Clear Damagers", "Silent Kill", "Add Behavior"
]);

export const ALL_STANDARD_STATUS = new Set([
  "Confused", "Terrorized", "Held", "Immobilized", "Stunned", "Sleep",
]);

export const ALL_KNOCK = new Set([
  "Knockback", "Knockup", "Repel"
]);

const NO_PREPOSITION = new Set(["Knockback"]);

function numberToStr(number, signed, sig_digits) {

  if (signed === undefined) signed = true;

  if (sig_digits === undefined) sig_digits = 3;

  let sign = signed ? (number >= 0 ? "+" : "") : "";
  return `${sign}${number.toPlaces(sig_digits)}`;
}

function tick_duration_and_delay(attrmod) {

  let effect = "";

  if (attrmod.duration.endsWith("seconds") || attrmod.duration_expression) {
    let duration;
    if (attrmod.duration_expression) {
      effect = "for [DurExpr]";
    }
    else {
      duration = parseFloat(attrmod.duration.split(" ")[0]);
      if (duration > 0.01) {
        let period = attrmod.application_period.toPlaces(3);
        if (period > 0.01) {
          effect = `every ${period}s for ${duration}s`;
        }
        else {
          effect = `for ${duration}s`;
        }
        let chance = attrmod.tick_chance;
        if (period > 0.01) {
          effect = `${effect} (${(chance * 100).toPlaces(1)}% chance)`;
        }
      }
    }
  }
  else if (attrmod.duration === "UntilKilled") {
    effect = "until caster defeated";
  }
  if (attrmod.delay) {
    effect = `${effect} after ${attrmod.delay.toPlaces(3)}s`;
  }
  return effect;
}


// Create a textual display of an attribmod's effects
export function display_attrmod(attrmod, is_activation, at_scales, level) {

  let attrib = "";
  let effect = "";
  let duration = "";
  let strength = "";
  let qualified = "";
  let target = "";

  let isDamage = false;
  let isDefense = false;
  let isRawScale = false;

  const prettyTable = attrmod.table;
  const table = prettyTable.toLowerCase();

  const aspect = attrmod.aspect;
  const params = attrmod.params;
  const scale = attrmod.scale;

  level = level ? (typeof level === "string" ? level.parseInt() : level) : 50;

  function asExpression(numeric_str, scale_places)  {

    if (attrmod.magnitude_expression) {
      return "[MagExpr]";
    }
    if (at_scales === undefined ||
        at_scales[table] === undefined ||
        at_scales[table][level-1] === undefined) {
      const places = (scale_places === undefined ? 3 : scale_places);
      return `(${scale.toPlaces(places)} * ${prettyTable})`;
    }
    return numeric_str;
  }

  function scaleToValue() {

    if (at_scales === undefined ||
        at_scales[table] === undefined ||
        at_scales[table][level-1] === undefined) {
        return scale;
    }
    return at_scales[table][level-1] * scale;
  }

  // Enforce self-only effect displays for attribmods inside
  // activation effects, no matter what the mods themselves say
  target = is_activation ? "self only" : TARGET_STRINGS[attrmod.target];

  const all_attribs = attrmod.attribs.map(a => a.replace(/_/g, " ")).map(a => {
    return a === "Healing Dmg" ? "Healing" : a;
  });

  // If this is a single attribute, note whether it one of several types of
  // attribute and move on
  if (all_attribs.length === 1) {
    attrib = all_attribs[0];
    isDamage = ALL_DAMAGE.has(attrib);
    isDefense = ALL_DEFENSE.has(attrib);
    isRawScale = (
      (ALL_STANDARD_STATUS.has(attrib) || ALL_KNOCK.has(attrib)) &&
      attrmod.aspect !== "Resistance"
    );
    // Trim the '_Dmg' from the end of the attrib
    if (isDamage) attrib = attrib.slice(0,-4);
  }
  // Group attributes that come in lists like damage, defense and
  // status attributes (for mezzes and similar status effects)
  else {
    // All damage types
    if (setsEqual(ALL_STD_DAMAGE, all_attribs)) {
      switch (aspect) {
        case "Strength": case "Current":
          isDamage = true;
          attrib = "Damage (All)";
          break;
        case "Resistance":
          attrib = "Damage Resistance (All)";
          break;
      }
    }
    // A subset of damage types
    else if (all_attribs.every(a => ALL_DAMAGE.has(a))) {
      const grouped = all_attribs.map(a => a.slice(0,-4)).join(", ");
      switch (aspect) {
        case "Strength": case "Current":
          isDamage = true;
          attrib = `Damage (${grouped})`;
          break;
        case "Resistance":
          attrib = `Damage Resistance (${grouped})`;
          break;
      }
    }
    // All defense types
    else if (setsEqual(ALL_DEFENSE, all_attribs)) {
      isDefense = true;
      switch (aspect) {
        case "Current":
          attrib = "Defense (All)";
          break;
        case "Strength":
          attrib = "Defense Strength (All)";
          break;
        case "Resistance":
          attrib = "Defense Resistance (All)";
          break;
      }
    }
    // A subset of defense types
    else if (all_attribs.every(a => ALL_DEFENSE.has(a))) {
      isDefense = true;
      const grouped = all_attribs.join(", ");
      switch (aspect) {
        case "Current":
          attrib = `Defense (${grouped})`;
          break;
        case "Strength":
          attrib = `Defense Strength (${grouped})`;
          break;
        case "Resistance":
          attrib = `Defense Resistance (${grouped})`;
          break;
      }
    }
    // All standard mez types
    else if (setsEqual(ALL_STANDARD_STATUS, all_attribs)) {
      switch (aspect) {
        case "Current":
          isRawScale = true;
          attrib = "Mez Protection (All)";
          break;
        case "Strength":
          attrib = "Mez Strength (All)";
          break;
        case "Resistance":
          attrib = "Mez Resistance (All)";
          break;
      }
    }
    // A subset of standard mez types
    else if (all_attribs.every(a => ALL_STANDARD_STATUS.has(a))) {
      const grouped = all_attribs.join(", ");
      switch (aspect) {
        case "Current":
          isRawScale = true;
          attrib = `Mez Protection (${grouped})`;
          break;
        case "Strength":
          attrib = `Mez Strength (${grouped})`;
          break;
        case "Resistance":
          attrib = `Mez Resistance (${grouped})`;
          break;
      }
    }
    // A subset of standard knock types
    else if (all_attribs.every(a => ALL_KNOCK.has(a))) {
      const grouped = all_attribs.join(", ");
      switch (aspect) {
        case "Current":
          isRawScale = true;
          attrib = `${grouped} Protection`;
          break;
        case "Strength":
          attrib = `${grouped} Strength`;
          break;
        case "Resistance":
          attrib = `${grouped} Resistance`;
          break;
      }
    }
    else {
      attrib = all_attribs.join(", ");
    }
  }

  // Aspect "absolute" is most-often used for gauge-like attribues
  // such as HP and endurance, which are totals which only change
  // when an instantaneous event moves them.
  if (attrmod.aspect === "Absolute" &&
      attrmod.type !== "Duration" &&
      !DIRECT_EFFECT.has(attrib))
  {
    let numeric;
    if (isDamage) {
      numeric = -1.0 * scaleToValue();
      strength = asExpression(numeric.toPlaces(4), 4);
      effect = `${strength} points of ${attrib} damage (${target})`;
    }
    else if (attrib.startsWith("Heal")) {
      numeric = scaleToValue();
      strength = asExpression(numeric.toPlaces(4), 4);
      effect = `Heal ${strength} points of damage (${target})`;
    }
    else {
      numeric = scaleToValue();
      strength = asExpression(numeric.toPlaces(3));
      effect = `${strength} points of ${attrib} (${target})`;
    }

    let timing = tick_duration_and_delay(attrmod);
    if (timing) effect = `${effect} ${timing}`;
    return effect;
  }
  // Affects that inflict Current aspect damage appply their damage as a
  // percentage of your max health
  else if (attrmod.aspect === "Current" && (isDamage || attrib === "Healing")) {
    if (isDamage) {
      let numeric = -1.0 * scaleToValue() * 100;
      strength = asExpression(`${numeric.toPlaces(4)}`, 4);
      if (at_scales === undefined) {
        effect = `${strength} * 100% max health in damage (${target})`;
      }
      else {
        effect = `${strength}% max health ${attrib} damage (${target})`;
      }
    }
    else if (attrib.startsWith("Healing")) {
      let numeric = scaleToValue() * 100;
      strength = asExpression(`${numeric.toPlaces(4)}`, 4);
      if (at_scales === undefined) {
        effect = `Heal ${strength} * 100% max health in damage (${target})`;
      }
      else {
        effect = `Heal ${strength}% max health damage (${target})`;
      }
    }

    let timing = tick_duration_and_delay(attrmod);
    if (timing) effect = `${effect} ${timing}`;
    return effect;
  }

  // Some "Null" attrib effects exist as carriers for visual effects
  // This usually happens when different conditions apply to different
  // effects being shown, so the FX can't always be lumped with the
  // regular attribmods
  if (attrib === "Null") {
    const fx = attrmod.fx;
    if (fx !== null) {
      if (attrmod.fx.continuing_fx) {
        strength = "Continuing";
        attrib = `FX "${attrmod.fx.continuing_fx}"`;
      }
      else if (attrmod.fx.conditional_fx) {
        strength = "Conditional";
        attrib = `FX "${attrmod.fx.conditional_fx}"`;
      }
      effect = `${strength} ${attrib}`;
    }
    else {
      effect = "Null";
    }
    return effect;
  }

  // This primarily applies to status effects. Their scale determines their
  // duration and their magnitude is what we commonly call "Mag".
  if (attrmod.type === "Duration") {
    let numeric = scaleToValue();
    strength = asExpression(`${numeric.toPlaces(3)}`);
    effect = `${strength} second ${attrib} (Mag ${attrmod.magnitude})`;
    return effect;
  }

  let numeric = scaleToValue();
  if (attrmod.aspect === "Absolute") {
    strength = asExpression(`${numeric.toPlaces(3)}`);
  }
  else if (attrib.startsWith("StealthRadius")) {
    strength = `${asExpression(numberToStr(numeric))} feet`;
  }
  else if (attrib.startsWith("Perception") && attrmod.aspect === "Current") {
    const feet = numeric * (at_scales ? at_scales.base_perception: 1);
    strength = `${asExpression(numberToStr(feet))} feet`;
  }
  else if (["Absorb", "HitPoints"].includes(attrib)) {
    strength = `${asExpression(numberToStr(numeric, false, true))}`;
  }
  else if (isRawScale ) {
    strength = `Mag ${asExpression(numberToStr(numeric))}`;
  }
  else {
    strength = `${asExpression(numberToStr(numeric * 100))}%`;
  }

  // Handle duration
  duration = tick_duration_and_delay(attrmod);

  if (isDefense && !attrib.includes("Defense")) {
    switch (aspect) {
      case "Current":
        qualified = `Defense (${attrib})`;
        break;
      case "Strength":
        qualified = `Defense Strength (${attrib})`;
        break;
      case "Resistance":
        qualified = `Defense Resistance (${attrib})`;
        break;
    }
  }
  else if (attrmod.aspect === "Maximum") {
    qualified = `maximum ${attrib}`;
  }
  else if (attrmod.aspect === "Strength" && !attrib.includes("Strength")) {
    qualified = `${attrib} Strength`;
  }
  else if (attrmod.aspect === "Resistance" && !attrib.includes("Resistance")) {
   qualified = `Resistance (${attrib})`;
  }
  else if (attrmod.aspect === "Absolute" && attrib !== "Null" && !attrib.includes("points")) {
    qualified = `points of ${attrib}`;
  }
  else if (attrib === "Global_Chance_Mod") {
    if (params && (params.type === "EffectFilter") && checkArray(params.tags)) {
      qualified = `${attrib} to effects tagged ${params.tags.join(" | ")}`;
    }
  }
  else {
    qualified = attrib;
  }

  if (DIRECT_EFFECT.has(attrib)) {

    switch(attrib) {
      case "Set Mode":
      case "Unset Mode":
        effect = `${attrib} (${attrmod.mode_name}) ${duration}`;
        break;
      case "Add Behavior":
        let behavior;
        try {
          const behaviors = attrmod.params.behaviors;
          behavior = behaviors.map(b => b.split("(")[0]).join(",");
        }
        catch(exp) {
          behavior = "Unknown";
        }
        effect = `${attrib} (${behavior}) ${duration}`;
        break;
      case "Create Entity":
        const entity = params.entity_def ? params.entity_def : "Pseudopet";
        if (table !== "ranged_ones") {
          strength = asExpression(scaleToValue().toPlaces(3));
          effect = `${attrib} (level ${strength}, priority '${params.priorities}') ${duration}`;
        }
        else {
          effect = `${attrib} (level ${level}, priority '${params.priorities}') ${duration}`;
        }
        break;
      case "Recharge Power":
        const flags = attrmod.flags.map(x => x.split(" ")[0]);
        if (flags.includes('SetTimer')) {
          strength = asExpression(scaleToValue().toPlaces(1));
          effect = `Apply "${attrib} in ${strength} sec" ${duration}`;
        }
        else {
          // Same as default case below
          effect = `Apply "${attrib}" ${duration}`;
        }
        break;
      default:
        effect = `Apply "${attrib}" ${duration}`;
    }
  }
  else if (NO_PREPOSITION.has(attrib)) {
    effect = `${strength} to ${qualified} ${duration}`;
  }
  else {
    effect = `${strength} ${qualified} ${duration}`;
  }

  // if (attrmod.delay) {
  //   return `${effect} (${target}) after ${attrmod.delay.toPlaces(3)}s`;
  // }

  if (attrmod.application_type !== "OnTick") {
    console.log(attrmod.application_type);
    const appl_type = attrmod.application_type.slice(2).toLowerCase();
     return `${effect} (${target}) on ${appl_type}`;
  }

  return `${effect} (${target})`;
}
