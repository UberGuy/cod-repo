/*jshint esversion: 8 */

import * as settings from "/js/settings.js";
import * as display from "/components/cod-effect/libs/display_effect.js";
import { fetch_template } from "/js/utils.js";
import { PVE_OR_PVP } from "/components/cod-power/libs/effect_object.js";
import urls_mixin from "/components/cod-links/libs/urls_mixin.js";

import ModIcons from "/components/cod-mod-icons/comp.js";
import EffectIcons from "/components/cod-effect-icons/comp.js";

// Return a Promise so we can retrieve the template externally
export default async () => {

  const template = await fetch_template("cod-effect");
  return {
    name: "effect-tree",
    template: template,
    mixins: [urls_mixin],
    components: {
      "mod-icons": ModIcons,
      "effect-icons": EffectIcons,
    },
    props: {
      effect_data: {
        type: Object,
        required: true,
      },
      parent_data: {
        type: Object,
        default: null,
      },
      query_at: {
        type: String,
        default: null,
      },
      is_activation: {
        type: Boolean,
        default: false,
      },
      at_tables: {
        type: Object,
        default: undefined,
      },
      display_level: {
        type: Number,
        default: 50,
      },
      show_pvp_or_pve: {
        type: String,
        default: PVE_OR_PVP.EITHER,
      },
    },
    data() {
      return {
        display_100_pct: true,
        mounted: false,
      };
    },
    methods: {
      display_attrmod(attrmod) {
        return display.display_attrmod(
          attrmod,
          this.is_activation,
          this.at_tables,
          this.display_level,
        );
      },
      data_defined(attrmod) {
        const table_name = attrmod.table.toLowerCase();
        return (
          table_name &&
          this.at_tables &&
          this.at_tables[table_name] &&
          (this.at_tables[table_name][this.display_level -1] !== undefined)
        );
      },
      has_power_list(attrmod) {
        return attrmod.params && attrmod.params.type === "Power";
      },
      magnitude_expression(attrmod) {
        if (this.at_tables) {
          const table = attrmod.table.toLowerCase();
          const sign = table.endsWith("damage") ? -1 : 1;
          const stdres = sign * (this.at_tables[table][this.level_offset] * attrmod.scale).toPlaces(3);
          return attrmod.magnitude_expression
            .replace(/@StdResult/ig, stdres)
            .replace(/@Scale/ig, attrmod.scale);
        }
        // Figure out number of decimal places to display
        const attribs = attrmod.attribs;
        const places = attribs.every(a => display.ALL_DAMAGE.has(a)) ? 4 : 3;

        return attrmod.magnitude_expression
        .replace(
          /@StdResult/ig,
          `(${attrmod.scale.toPlaces(places)}*${attrmod.table})`)
        .replace(/@Scale/ig, attrmod.scale);
      },
      rewards(attrmod) {
        if (attrmod.attribs.length == 1 && attrmod.attribs[0] === "Reward") {
          return attrmod.params.rewards;
        }
        return [];
      },
      entity_redirects(attrmod) {
        if (attrmod.attribs.length == 1 && attrmod.attribs[0] === "Create_Entity") {
          if (attrmod.params) {
            return attrmod.params.redirects || [];
          }
        }
        return [];
      },
      entity_summons(attrmod) {
        // Returns an array for a result that's always a singleton
        // so we can use that result with `v-for` and avoid double
        // invocation with `v-if ... {{value}}`
        if (
          attrmod.attribs.length == 1 &&
          attrmod.attribs[0] === "Create_Entity" &&
          attrmod.params &&
          attrmod.params.entity_def
        ) {
          return [attrmod.params.entity_def];
        }
        return [];
      },
      effect_always_happens(effect_data) {
        return (effect_data.chance === 1.0 && effect_data.ppm === 0);
      },
      display_effect_chance(effect_data) {
        const display_100_pct = settings.get_setting("display_100_pct");
        return !this.effect_always_happens(effect_data) || display_100_pct;
      }
    },
    computed: {
      pvp_or_pve() {
        // If the parent is flagged as PVP_ONLY or PVE_ONLY, and

        let parent_pvp;
        if (this.parent_data) parent_pvp = this.parent_data.pvp_or_pve();
        else parent_pvp = PVE_OR_PVP.EITHER;

        let this_pvp = this.effect_data.pvp_or_pve();
        // If the parent is EITHER, unconditionally
        // return this effect's setting
        if (parent_pvp === PVE_OR_PVP.EITHER) {
          return this_pvp;
        }

        // Override the parent and this effect's
        // PVP setting if it is not EITHER
        if (this_pvp !== PVE_OR_PVP.EITHER) {
          return this_pvp;
        }
        return parent_pvp;
      },
      is_pvp_only() {
        return this.pvp_or_pve === PVE_OR_PVP.PVP_ONLY;
      },
      is_pve_only() {
        return this.pvp_or_pve === PVE_OR_PVP.PVE_ONLY;
      },
      is_either_pve_or_pvp() {
        return this.pvp_or_pve === PVE_OR_PVP.EITHER;
      },
      level_offset() {
        return this.display_level - 1;
      },
    },
    mounted() {
      this.display_100_pct = settings.get_setting("display_100_pct");
      this.mounted = true;
    },
    watch: {
      display_100_pct: function(value) {
        if (this.mounted) {
          settings.set_setting("display_100_pct", value);
        }
      },
    },
  };
};
