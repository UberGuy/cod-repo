/*jshint esversion: 8 */

export default {

  computed: {
    // Default options for Tippy display
    TIPPY_DEFAULTS() {
      return {
        arrow: true,
        allowHTML: true,
        interactive: false,
        ignoreAttributes: true,
        animation: 'scale',
        theme: 'cod',
      };
    },

    TIPPY_INTERACTIVE() {
      const options = Object.assign({}, this.TIPPY_DEFAULTS);
      options.interactive = true;
      return options;
    },
  }
};
