/*jshint esversion: 6 */

import * as settings from "/js/settings.js";

function CodConfig() {

  const that = Object.create(CodConfig.prototype);
  // The path used by the page scripts to locate
  // the root of the JSON data bundle.
  return that;
}

CodConfig.prototype = {

  get data_path() {
    const fromSettings = settings.get_setting("data_path");

    if (fromSettings) {
      return fromSettings;
    }

    if (location.hostname === "cod.uberguy.net") {
      return "/homecoming";
    }

    return "/cryptic";
  },
};

export const CONFIG = CodConfig();
